package com.luxoft.skurski.bankapp.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.luxoft.skurski.bankapp.util.Validation;

public class ValidationTest {

	@Test (expected=NullPointerException.class)
	public void checkForNullTest() {
		Validation.checkForNull(null);
	}
	
	@Test
	public void checkIsEmailTest() {
		assertTrue(Validation.checkIsEmail("piotr@wp.pl"));
		
		assertFalse(Validation.checkIsEmail("piotr@pl"));
		assertFalse(Validation.checkIsEmail("r@pl.pl"));
		assertFalse(Validation.checkIsEmail("piotr@2.pl.pl"));
	}
	
	@Test
	public void checkIsFullNameTest() {
		assertTrue(Validation.checkIsFullName("John Smith"));
		
		assertFalse(Validation.checkIsFullName("John"));
		assertFalse(Validation.checkIsFullName("John S"));
		assertFalse(Validation.checkIsFullName("john Smith"));
		assertFalse(Validation.checkIsFullName("John smith"));
		assertFalse(Validation.checkIsFullName("J Smith"));
	}
	
	@Test
	public void checkIsPhoneTest() {
		assertTrue(Validation.checkIsPhone("345-345-345"));
		assertTrue(Validation.checkIsPhone("345345345"));
		assertTrue(Validation.checkIsPhone("48345345345"));
		
		assertFalse(Validation.checkIsPhone("345 345 345"));
		assertFalse(Validation.checkIsPhone("122345345345"));
		assertFalse(Validation.checkIsPhone("k345345345"));
	}
	
	@Test
	public void checkIsWholeNumberTest() {
		assertTrue(Validation.checkIsWholeNumber("45"));
		
		assertFalse(Validation.checkIsWholeNumber("k"));
		assertFalse(Validation.checkIsWholeNumber("0.1"));
		assertFalse(Validation.checkIsWholeNumber("-5"));
	}
	
	@Test
	public void checkIsPositiveNumberTest() {
		assertTrue(Validation.checkIsPositiveNumber("100"));
		assertTrue(Validation.checkIsPositiveNumber("0.5"));
		
		assertFalse(Validation.checkIsPositiveNumber("-1000.95"));
		assertFalse(Validation.checkIsPositiveNumber("-100"));
		assertFalse(Validation.checkIsPositiveNumber("test"));
		assertFalse(Validation.checkIsPositiveNumber("1f"));
	}
	
	@Test
	public void checkIsExpectedNumberTest() {
		assertTrue(Validation.checkIsExpectedNumber("2",4));
		assertTrue(Validation.checkIsExpectedNumber("0",1));
		assertTrue(Validation.checkIsExpectedNumber("0",0));
		
		assertFalse(Validation.checkIsExpectedNumber("2", 1));
		assertFalse(Validation.checkIsExpectedNumber("0.5", 1));
		assertFalse(Validation.checkIsExpectedNumber("-2", 1));
	}
	
}
