package com.luxoft.skurski.bankapp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.model.SavingAccount;
import com.luxoft.skurski.bankapp.service.BankService;
import com.luxoft.skurski.bankapp.service.BankServiceImpl;

public class BankServiceTest {
	private Bank bank;
	private BankService bankService;
	private Client client;
	
	@Before
	public void setUp() throws ClientExistsException {
		bank = new Bank();
		bankService = new BankServiceImpl();
		client = new Client("John", Gender.MALE, 500f);
		bankService.addClient(bank, client);
	}
	
	@Test
	public void addClientTest() {
		Client client1 = new Client("Tester", Gender.MALE, 1000f);
		Client client2 = new Client("Travolta", Gender.MALE, 1000f);
		Client client3 = new Client("Tester", Gender.MALE, 1000f);
		try {
			bankService.addClient(bank, client1);
			bankService.addClient(bank, client2);
			bankService.addClient(bank, client3);
		} catch (ClientExistsException ex) {
			System.out.println(ex.printMessage());
		}
		
		assertTrue(client1.equals(client3));
		assertEquals(3, bank.getClients().size());
		assertThat(bank.getClients().size(), is(not(4)));
	}
	
	@Test
	public void removeClientTest() throws ClientExistsException {
		bankService.addClient(bank, new Client("Another", Gender.FEMALE, 1f));
		bankService.removeClient(bank, client);
		
		assertEquals(1, bank.getClients().size());
		assertFalse(bank.getClients().contains(client));
	}
	
	@Test
	public void addAccountTest() {
		Account account = new CheckingAccount(100f, 0f);
		bankService.addAccount(client, account);
		
		assertTrue(new ArrayList<Client>(bank.getClients()).get(0).getAccounts().contains(account));
	}
	
	@Test
	public void setActiveAccountTest() {
		Account checkingAccount = new CheckingAccount(1000f,0f);
		Account savingAccount = new SavingAccount(9999f);
		
		client.addAccount(checkingAccount);
		client.addAccount(savingAccount);
		
		bankService.setActiveAccount(client, checkingAccount);
		
		assertTrue(new ArrayList<Client>(bank.getClients()).get(0).getActiveAccount().equals(checkingAccount));
	}
	
	@Test
	public void getClientTest() throws ClientNotExistsException {
		assertTrue(client.equals(bankService.getClient(bank, "John")));
	}
	
	@Test (expected=ClientNotExistsException.class)
	public void getClientNotExistsException() throws ClientNotExistsException {
		bankService.getClient(bank, "null");
	}
	
	@Test
	public void saveAndLoadClientTest() {
		Client client = new Client("Skurski", Gender.MALE, 5432f, "skurski@gmail.com",
							"294-543-234", "Cracov");
		
		bankService.saveClient(client);
		Client recoverClient = bankService.loadClient();
		
		assertTrue(client.equals(recoverClient));
	}
}
