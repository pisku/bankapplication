package com.luxoft.skurski.bankapp.dao;

import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class AccountDAOImplTest {
    private Client client;
    private Client client2;
    private List<Account> accounts = new ArrayList<Account>();
    private FillDB fillDB = new FillDB();
    private BaseDAO db = new BaseDAOImpl();
    private AccountDAO accountDAO = new AccountDAOImpl();

    @Before
    public void setUp() throws DAOException {
        fillDB.setTestEnv(true);
        fillDB.init();
        db.setTestEnv(true);
        ((BaseDAOImpl) accountDAO).setTestEnv(true);

        client = new Client("Piotr Tester88", Gender.MALE,550,"peter@tester.pl","111-222-333","Cracov");
        client2 = new Client("Test Drugi",Gender.MALE,999,"drugi@test.pl","543-534-533","Katowice");

        int clientId = db.insert(
               "client",
                new String[] {
                     "bank_id","name","email","phone","city","gender","initial_overdraft"
                },
                new String[] {
                         "1",
                        client.getName(),
                        client.getEmail(),
                        client.getPhone(),
                        client.getCity(),
                        client.getGender().getSign(),
                        "550"
                }
        );
        client.setId(clientId);

        int clientId2 = db.insert(
                "client",
                new String[] {
                        "bank_id","name","email","phone","city","gender","initial_overdraft"
                },
                new String[] {
                        "1",
                        client2.getName(),
                        client2.getEmail(),
                        client2.getPhone(),
                        client2.getCity(),
                        client2.getGender().getSign(),
                        "999"
                }
        );
        client2.setId(clientId2);

        Account account1 = new CheckingAccount(1,clientId,4573f,550f);
        int accountId1 = db.insert(
               "account",
                new String[] {
                     "balance","overdraft","type","client_id"
                },
                new String[] {
                        String.valueOf(account1.getBalance()),
                        String.valueOf(account1.getOverdraft()),
                        "c",
                        String.valueOf(clientId)
                }
        );
        account1.setId(accountId1);

        Account account2 = new SavingAccount(1,clientId, 223f);
        int accountId2 = db.insert(
                "account",
                new String[] {
                        "balance","overdraft","type","client_id"
                },
                new String[] {
                        String.valueOf(account2.getBalance()),
                        String.valueOf(account2.getOverdraft()),
                        "s",
                        String.valueOf(clientId)
                }
        );
        account2.setId(accountId2);

        accounts.add(account1);
        accounts.add(account2);
    }

    @After
    public void cleanUp() throws DAOException {
//        for (Account account: accounts) {
//            delete("account", "id", String.valueOf(account.getId()));
//        }
//        delete("client", "id", String.valueOf(client.getId()));
//        delete("client", "id", String.valueOf(client2.getId()));
        fillDB.clean();
    }

    @Test
    public void getClientAccountsTest() throws DAOException {
        PreparedStatement stmt = null;
        ResultSet result = null;
        List<Account> searchedAccounts = null;

        searchedAccounts = accountDAO.getClientAccounts(client.getId());

        assertNotNull(searchedAccounts);
        assertTrue(searchedAccounts.size() == 2);
    }

    @Test
    public void addTest() throws DAOException {
        Account account = new CheckingAccount(1,client2.getId(),5000,9999);
        accountDAO.add(account);
        PreparedStatement stmt = null;
        ResultSet result = null;

        try {
            stmt = db.openConnection().prepareStatement(
                    "SELECT id,balance,overdraft,type,client_id " +
                    "FROM account " +
                    "WHERE client_id = ?"
            );
            stmt.setInt(1, client2.getId());
            result = stmt.executeQuery();

            assertTrue(result.next());
            assertTrue((account.getBalance() == result.getFloat("balance")));

            stmt = db.openConnection().prepareStatement(
                    "DELETE FROM account WHERE client_id = ?"
            );
            stmt.setInt(1, client2.getId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }

    @Test
    public void saveTest() throws DAOException {
        Account accountToUpdate = new CheckingAccount(accounts.get(0).getId(),
                client.getId(), 12345f, 9876f);

        accountDAO.save(accountToUpdate);

        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = db.openConnection().prepareStatement(
                    "SELECT id,balance,overdraft,type,client_id " +
                    "FROM account " +
                    "WHERE id = ?"
            );
            stmt.setInt(1, accounts.get(0).getId());
            result = stmt.executeQuery();

            assertTrue(result.next());
            assertTrue((accountToUpdate.getBalance() == result.getFloat("balance")));
            assertTrue((accountToUpdate.getOverdraft() == result.getFloat("overdraft")));
            assertTrue((accountToUpdate.getClientId() == result.getInt("client_id")));
            assertTrue((accountToUpdate.getId() == result.getInt("id")));

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }

    @Test
    public void removeByClientIdTest() throws DAOException {

        accountDAO.removeByClientId(client.getId());

        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = db.openConnection().prepareStatement(
                    "SELECT id,balance,overdraft,type,client_id " +
                    "FROM account " +
                    "WHERE client_id = ?"
            );
            stmt.setInt(1, client.getId());
            result = stmt.executeQuery();

            assertFalse(result.next());

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }
}
