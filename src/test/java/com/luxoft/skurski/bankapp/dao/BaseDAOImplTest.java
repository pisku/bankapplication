package com.luxoft.skurski.bankapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.DAOException;

public class BaseDAOImplTest {
    private FillDB fillDB = new FillDB();
    private BaseDAO db = new BaseDAOImpl();
    private BankDAO bankDao = new BankDAOImpl();

    @Before
    public void setUp() {
        fillDB.setTestEnv(true);
        fillDB.init();
        db.setTestEnv(true);
        ((BaseDAOImpl) bankDao).setTestEnv(true);
    }

    @After
    public void cleanUp() {
        fillDB.clean();
    }

	@Test
	public void connectionTest() throws DAOException {
		Statement stmt = null;
		ResultSet result = null;
		
		try {
			stmt = db.openConnection().createStatement();
			stmt.executeUpdate("create table test (id int auto_increment primary key, name varchar(255))");
			stmt.executeUpdate("insert into test values (1, 'first insert')");
			stmt.executeUpdate("insert into test values (2, 'second insert')");
			stmt.executeUpdate("insert into test values (3, 'third insert')");
			
			result = stmt.executeQuery("select * from test");
            System.out.println("Connection test: ");
			while (result.next()) {
                System.out.println("id: " + result.getInt(1) + ", name: " + result.getString(2));
			}
			
			stmt.executeUpdate("DROP TABLE test");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				result.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			db.closeConnection();
		}
	}
}
