package com.luxoft.skurski.bankapp.dao;

import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class ClientDAOImplTest {
    private FillDB fillDB = new FillDB();
    private BaseDAO db = new BaseDAOImpl();
    private ClientDAO clientDao = new ClientDAOImpl();
    private Bank bank = new Bank("PS Bank");

    @Before
    public void setUp() {
        fillDB.setTestEnv(true);
        fillDB.init();
        db.setTestEnv(true);
        ((BaseDAOImpl) clientDao).setTestEnv(true);
        bank.setId(1);
    }

    @After
    public void cleanUp() {
        fillDB.clean();
    }

    @Test
    public void findClientByNameTest() throws ClientNotExistsException, DAOException {
        Client client = clientDao.findClientByName(bank, "Piotr Skurski");

        assertNotNull(client);
        assertTrue(client.getName().equals("Piotr Skurski"));
        assertTrue(client.getId() == 1);
        assertTrue(client.getPhone().equals("111-222-333"));
        assertTrue(client.getCity().equals("Cracov"));
        assertTrue(client.getGender() == Gender.MALE);
        assertTrue(client.getEmail().equals("peter@gmail.com"));

        assertTrue(client.getAccounts().size() == 2);
    }

    @Test
    public void isClientInDBTest() {
        boolean isClient = ((ClientDAOImpl) clientDao).
                isObjectInDbByName("Piotr Skurski","client");

        assertTrue(isClient);
    }

    @Test
    public void saveTest() throws DAOException {
        Client newClient = new Client(0,1,"Monika Jakubas",Gender.MALE,500,
                            "jakubas@mon.pl","987654d332","Wroclaw");
        clientDao.save(newClient);

        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = db.openConnection().prepareStatement(
                    "SELECT id FROM client WHERE name=?"
            );
            stmt.setString(1, newClient.getName());
            result = stmt.executeQuery();

            assertTrue(result.next());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }

    @Test
    public void removeTest() throws DAOException {
        Client clientInDB = new Client(1,1,"Piotr Skurski",Gender.MALE,1000,
                            "peter@gmail.com","111-222-333","Cracov");
        clientDao.remove(clientInDB);

        PreparedStatement stmt = null;
        ResultSet result = null;

        try {
            stmt = db.openConnection().prepareStatement(
                    "SELECT id FROM client WHERE name=?"
            );
            stmt.setString(1, clientInDB.getName());
            result = stmt.executeQuery();

            assertFalse(result.next());

            stmt = db.openConnection().prepareStatement(
                    "SELECT id FROM account WHERE client_id=?"
            );
            stmt.setInt(1, clientInDB.getId());
            result = stmt.executeQuery();

            assertFalse(result.next());

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }

    @Test
    public void getAllClientsTest() throws DAOException {
        List<Client> clients = clientDao.getAllClients(bank);

        assertNotNull(clients);
        assertTrue(clients.size() == 5);
    }
}
