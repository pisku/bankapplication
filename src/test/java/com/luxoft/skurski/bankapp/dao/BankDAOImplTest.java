package com.luxoft.skurski.bankapp.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.model.*;
import com.luxoft.skurski.bankapp.test.service.TestService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.BankNotFoundException;
import com.luxoft.skurski.bankapp.exception.DAOException;

public class BankDAOImplTest {
    private FillDB fillDB = new FillDB();
    private BaseDAO db = new BaseDAOImpl();
    private BankDAO bankDao = new BankDAOImpl();
    private ClientDAO clientDAO = new ClientDAOImpl();
    private String bankName = "My Bank";
    private String bank2Name = "Another Bank";
	private Bank bank, bank2;

	@Before
	public void setUp() throws ClientExistsException {
        fillDB.setTestEnv(true);
        fillDB.init();
        db.setTestEnv(true);
        ((BaseDAOImpl) bankDao).setTestEnv(true);
        ((BaseDAOImpl) clientDAO).setTestEnv(true);

        bank = new Bank(bankName);
		Client client = new Client("Ivan Ivanow", Gender.MALE,1000);
		client.setCity("Kiev");
		client.addAccount(new CheckingAccount());
		bank.addClient(client);

        bank2 = new Bank(bank2Name);
        bank2.addClient(client);
	}

	@After
	public void cleanUp() {
		fillDB.clean();
	}

	@Test
	public void getBankByNameTest() throws BankNotFoundException {
		PreparedStatement stmt = null;
        ResultSet result = null;
        String name = "PS TestGetBank";
		Bank bank = new Bank(name);
		
		try {
            String sql = "INSERT INTO bank (name) VALUES (?)";
			stmt = db.openConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name);
            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("No bank inserted!");
            }

            result = stmt.getGeneratedKeys();
            if (result.next()) {
                bank.setId(result.getInt(1));
            }

			Bank searchedBank = bankDao.getBankByName(name);
            String delete = "DELETE FROM bank WHERE name = ?";
			stmt = db.openConnection().prepareStatement(delete);
            stmt.setString(1, name);
            stmt.executeUpdate();

            assertTrue(bank.equals(searchedBank));
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
                result.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			db.closeConnection();
		}
	}
	
	@Test (expected=BankNotFoundException.class)
	public void getBankByNameExceptionTest() throws DAOException, BankNotFoundException {
		bankDao.getBankByName("Not ExistsBank");
	}
	
	@Test
	public void saveBankTest() throws SQLException, BankNotFoundException {
		String name = "PS SaveBankTest";
		bankDao.save(new Bank(name));

		Statement stmt = null;
		ResultSet result = null;
		try {
			stmt = db.openConnection().createStatement();
			result = stmt.executeQuery("SELECT id,name FROM bank WHERE name='"+name+"'");
			assertNotNull(result);

			if (result.next()) {
				assertTrue(name.equals(result.getString(2)));
			}
			
			stmt.executeUpdate("DELETE FROM bank WHERE name='"+name+"'");
		} finally {
			result.close();
			stmt.close();
			db.closeConnection();
		}
	}
	
	@Test
	public void removeBankTest() throws DAOException {
		PreparedStatement stmt = null;
        ResultSet result = null;
        String name = "PS RemoveBankTest";
        int id = 99999;
        Bank bank = new Bank(name);
        bank.setId(id);

        try {
            stmt = db.openConnection().prepareStatement("INSERT INTO bank " +
                    "(id,name) VALUES (?,?)");
            stmt.setInt(1, id);
            stmt.setString(2, name);
            int affectedRows = stmt.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("No bank inserted.");
            }

            bankDao.remove(bank); // test remove

            stmt = db.openConnection().prepareStatement("SELECT id,name " +
                    "FROM bank WHERE id=?");
            stmt.setInt(1, id);
            result = stmt.executeQuery();
            assertFalse(result.next());

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            db.closeConnection();
        }
    }

	@ Test
	public void testInsert () throws DAOException, IllegalAccessException, BankNotFoundException {
        bankDao.saveAll(bank);

		Bank bank2 = bankDao.loadBank(bankName);

		assertTrue(TestService.isEquals(bank, bank2));
	}


	@ Test
	public void testUpdate () throws IllegalAccessException, DAOException, ClientExistsException, BankNotFoundException {
        bankDao.saveAll(bank2);

		// Make changes to Bank and save
		Client client2 = new Client("Ivan Petrov", Gender.MALE,1000);
		client2.setCity("New York");
		client2.addAccount(new SavingAccount());
        bank2.addClient(client2);
		bankDao.saveAll(bank2);

		Bank loadBank = bankDao.loadBank(bank2Name);

		assertTrue(TestService.isEquals(bank2, loadBank));
	}
}
