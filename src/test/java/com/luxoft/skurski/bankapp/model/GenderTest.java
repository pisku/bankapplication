package com.luxoft.skurski.bankapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.luxoft.skurski.bankapp.model.Gender;

public class GenderTest {

	@Test
	public void parseGenderTest() {
		assertEquals(Gender.MALE, Gender.parseGender("m"));
		assertEquals(Gender.FEMALE, Gender.parseGender("f"));
		assertFalse(Gender.FEMALE == Gender.parseGender("m"));
		assertNull(Gender.parseGender("x"));
	}
}
