package com.luxoft.skurski.bankapp.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.OverDraftLimitExceededException;
import com.luxoft.skurski.bankapp.model.CheckingAccount;

public class CheckingAccountTest {
	private CheckingAccount checkingAccount;
	
	@Before
	public void setUp() {
		checkingAccount = new CheckingAccount(1000f, 1000f);
	}

	@Test
	public void withdrawTest() {
		try {
			checkingAccount.withdraw(2000f);
		} catch (OverDraftLimitExceededException ex) {
			System.out.println(ex.printMessage());
		}
		float balance = checkingAccount.getBalance();
		assertEquals(-1000f, balance, 0f);
		assertFalse(-1000.1f == balance);
		assertFalse(-999.9f == balance);
	}
	
	@Test (expected=OverDraftLimitExceededException.class)
	public void withdrawOverDraftLimitExceededExceptionTest() throws OverDraftLimitExceededException {
		checkingAccount.withdraw(2000.1f);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void withdrawIllegalArgumentExceptionTest() throws OverDraftLimitExceededException {
		checkingAccount.withdraw(-0.1f);
	}
	
	@Test
	public void maximumAmountToWithdrawTest() {
		assertEquals(2000f, checkingAccount.maximumAmountToWithdraw(), 0f);
		assertFalse(2000.1f == checkingAccount.maximumAmountToWithdraw());
		assertFalse(1999.9f == checkingAccount.maximumAmountToWithdraw());
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void depositIllegalArgumentExceptionTest() {
		checkingAccount.deposit(-0.1f);
	}
	
	@Test
	public void decimalValueTest() {
		checkingAccount.setBalance(333.34567f);
		assertEquals(333.35f, checkingAccount.decimalValue(), 0f);
	}

}

