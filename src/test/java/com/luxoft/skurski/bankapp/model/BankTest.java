package com.luxoft.skurski.bankapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;

public class BankTest {
	private Bank bank;
	private Client testClient;

	@Before
	public void setUp() throws ClientExistsException {
		bank = new Bank();
		testClient = new Client("Smith", Gender.MALE, 1000f);
		bank.addClient(testClient);
	}
	
	@Test
	public void addClientTest() throws ClientExistsException {
		Client expectedClient = new Client("Tester", Gender.MALE, 1000f);
		bank.addClient(expectedClient);
		
		assertEquals(2, bank.getClients().size());
		assertTrue(bank.getClients().contains(expectedClient));
	}
	
	@Test (expected=ClientExistsException.class)
	public void addClientExistsExceptionTest() throws ClientExistsException {
		Client expectedClient = new Client("Tester", Gender.MALE, 1000f);
		bank.addClient(expectedClient);
		bank.addClient(expectedClient);
	}
	
	@Test
	public void removeClientTest() {
		Client removeClient = new Client("Smith", Gender.MALE, 1000f);
		bank.removeClient(removeClient);
		
		assertEquals(0, bank.getClients().size());
		assertFalse(bank.getClients().contains(removeClient));
	}
	
	@Test
	public void findClient() throws ClientNotExistsException {
		Client findClient = bank.findClient("Smith");
		
		assertTrue(testClient.equals(findClient));
	}
	
	@Test (expected=ClientNotExistsException.class)
	public void findClientNotExistsExceptionTest() throws ClientNotExistsException {
		bank.findClient("Null");
	}
}
