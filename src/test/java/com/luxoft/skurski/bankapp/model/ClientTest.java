package com.luxoft.skurski.bankapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.model.SavingAccount;

public class ClientTest {
	private Client testClient;
	private Account testCheckingAccount;
	
	@Before
	public void setUp() {
		testClient = new Client("Smith", Gender.MALE, 1000f);
		testCheckingAccount = new CheckingAccount(2000f, 0f);
	}

	@Test
	public void addAccountTest() {
		Account checkingAccount = new CheckingAccount(1000f, 500f);
		Account savingAccount = new SavingAccount(5000f);
		System.out.println(checkingAccount);
		testClient.addAccount(checkingAccount);
		testClient.addAccount(savingAccount);
		
		assertEquals(2, testClient.getAccounts().size());
		assertTrue(testClient.getAccounts().contains(checkingAccount));
		assertTrue(testClient.getAccounts().contains(savingAccount));
		assertFalse(testClient.getActiveAccount().equals(checkingAccount));
		assertTrue(testClient.getActiveAccount().equals(savingAccount));
	}
	
	@Test
	public void searchAccountTest() {
		int id = testCheckingAccount.getId();
		testClient.addAccount(testCheckingAccount);
		Account savingAccount = new SavingAccount(666f);
		int newId = savingAccount.getId();
		testClient.addAccount(savingAccount);
		
		assertTrue(testClient.searchAccount(id).equals(testCheckingAccount));
		assertFalse(testClient.searchAccount(newId).equals(testCheckingAccount));
		assertNull(testClient.searchAccount(id+100));
	}
}
