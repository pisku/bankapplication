package com.luxoft.skurski.bankapp.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;
import com.luxoft.skurski.bankapp.model.SavingAccount;

public class SavingAccountTest {
	private SavingAccount savingAccount;
	
	@Before
	public void setUp() {
		savingAccount = new SavingAccount(1000f);
	}

	@Test
	public void setBalanceTest() {
		float balance = 1000f;
		savingAccount.setBalance(balance);

		assertEquals(savingAccount.getBalance(), balance, 0.1f);
		assertFalse(savingAccount.getBalance() == balance+0.1f);
		assertFalse(savingAccount.getBalance() == balance-0.1f);
	}
	
	@Test
	public void withdrawTest() {
		float balance = 1000f;
		float withdraw = 1000f;
		savingAccount.setBalance(balance);
		try {
			savingAccount.withdraw(withdraw);
		} catch (NotEnoughFundsException ex) {
			System.out.println(ex.printMessage());
		}
		assertEquals(savingAccount.getBalance(), 0f, 0f);
		assertFalse(savingAccount.getBalance() == 0.1f);
		assertFalse(savingAccount.getBalance() == -0.1f);
	}
	
	@Test (expected=NotEnoughFundsException.class)
	public void withdrawNotEnoughFundsExceptionTest() throws NotEnoughFundsException {
		savingAccount.withdraw(1000.1f);
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void setBalancellegalArgumentExceptionTest() {
		savingAccount.setBalance(-0.1f);
	}
}

