package com.luxoft.skurski.bankapp.test.service;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestServiceTest {
    Bank bank1, bank2, bank3;

    @Before
    public void initBanks () throws ClientExistsException {
        bank1 = new Bank();
        bank1.setId(1);
        bank1.setName("My Bank");
        Client client = new Client
                ("Joanna Kawa", Gender.FEMALE,500,"joanna@kawa.pl","123543654","Cracov");
        client.addAccount(new CheckingAccount());
        bank1.addClient(client);

        bank2 = new Bank();
        bank2.setId(2);
        bank2.setName("My Bank");
        Client client2 = new Client
                ("Joanna Kawa", Gender.FEMALE,500,"joanna@kawa.pl","123543654","Cracov");
        client2.addAccount(new CheckingAccount ());
        bank2.addClient(client2);

        bank3 = new Bank();
        bank3.setId(100);
        bank3.setName("Tester");
        Client client3 = new Client
                ("Joanna Kawa", Gender.FEMALE,500,"joanna@kawa.pl","123543654","Cracov");
        client3.addAccount(new CheckingAccount ());
        bank3.addClient(client3);

    }

    @Test
    public void testEquals () throws IllegalAccessException {
        assertTrue(TestService.isEquals(bank1, bank2));
        assertFalse(TestService.isEquals(bank2, bank3));
    }
}