package com.luxoft.skurski.bankapp.command;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import com.luxoft.skurski.bankapp.command.UIForm;
import com.luxoft.skurski.bankapp.model.Gender;

public class UIFormTest {

	@Test
	public void getFullNameTest() throws IOException {
		String inputData = "Piotr Skurski";
		System.setIn(new java.io.ByteArrayInputStream(inputData.getBytes()));
		String name = UIForm.getFullName();
		
		assertTrue(inputData.equals(name));
	}
	
//	@Test
	public void getEmailTest() throws IOException {
		String inputData = "piotr.skurski@wp.pl";
		System.setIn(new java.io.ByteArrayInputStream(inputData.getBytes()));
		String email = UIForm.getEmail();
		assertTrue(inputData.equals(email));
	}
	
//	@Test
	public void getGenderTest() throws IOException {
		String inputData = "0";
		System.setIn(new java.io.ByteArrayInputStream(inputData.getBytes()));
		Gender gender = UIForm.getGender();
		
		assertTrue(Gender.MALE == gender);
	}
	
}
