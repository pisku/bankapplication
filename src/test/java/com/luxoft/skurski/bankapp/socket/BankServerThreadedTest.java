package com.luxoft.skurski.bankapp.socket;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.service.BankFeedService;
import com.luxoft.skurski.bankapp.service.BankServiceImpl;
import com.luxoft.skurski.bankapp.testfeed.TestFeed;

public class BankServerThreadedTest {
	private Bank bank;
	private Client client;
	private Account account;
	
	@Before 
	public void setUp() {
		bank = new Bank();
		client = new Client("Piotr Tester", Gender.MALE, 1000, "peter@wp.pl", "111222333", "Cracow");
		account = new CheckingAccount(9999);
		
		TestFeed.createFolderWithFeedFiles();
		new BankFeedService(bank).loadFeed("src/main/resources/feed");
	}

	@Test
	public void testBankClientRunnable() throws ClientNotExistsException, InterruptedException, ClientExistsException {
		final int NUMBER_OF_THREADS = 1000;
		final float WITHDRAW_AMOUNT = 1f;
		final int SERVER_PORT = 2004;
		
        BankServiceImpl bankService = new BankServiceImpl();
        bankService.addClient(bank, client);
        bankService.addAccount(client, account);
        bankService.setActiveAccount(client, account);
        
        float initialBalance = account.getBalance();

		BankServerThreaded bankServer = new BankServerThreaded(bank, 2004);
		System.out.println("bankserver: " + bankServer);
		Thread serverThread = new Thread(bankServer);
        serverThread.start();

        List<Thread> clientMockThreadList = new ArrayList<Thread>(NUMBER_OF_THREADS);
        
        for (int i=0; i<NUMBER_OF_THREADS; i++) {
        	clientMockThreadList.add(new Thread(new BankClientMock(SERVER_PORT, client, WITHDRAW_AMOUNT)));
        }
        
		for (int i = 0; i <NUMBER_OF_THREADS; i ++) {
			clientMockThreadList.get(i).start();
		}
		
		for (int i = 0; i <NUMBER_OF_THREADS; i ++) {
			clientMockThreadList.get(i).join();
		}
		
		bankServer.setRunning(false);
		
		float endBalance = client.getBalance();
		
		assertEquals(initialBalance-NUMBER_OF_THREADS, endBalance, 0);
	}
	
	@Test
	public void testBankClientCallable() throws InterruptedException, ExecutionException, ClientExistsException {
		final int NUMBER_OF_THREADS = 1000;
		final float WITHDRAW_AMOUNT = 1f;
		final int SERVER_PORT = 2005;
		
        BankServiceImpl bankService = new BankServiceImpl();
        bankService.addClient(bank, client);
        bankService.addAccount(client, account);
        bankService.setActiveAccount(client, account);
        
        float initialBalance = account.getBalance();
		
        BankServerThreaded bankServer = new BankServerThreaded(bank, 2005);
        Thread serverThread = new Thread(bankServer);
        serverThread.start();

		ExecutorService executor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
		
		List<Future<Long>> clientMockList = new ArrayList<Future<Long>>(NUMBER_OF_THREADS);
		
		for(int i = 0; i < NUMBER_OF_THREADS; i++) {
			Callable<Long> callable = new BankClientMockCallable(SERVER_PORT, client, WITHDRAW_AMOUNT);
			Future<Long> future = executor.submit(callable);
			clientMockList.add(future);
		}
		executor.shutdown();
		
		try {
			executor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		executor.shutdownNow();
		
		long timeOfAllConnections = 0l;
		for (Future<Long> future : clientMockList) {
			timeOfAllConnections += future.get();
		}
		
		bankServer.setRunning(false);
		System.out.println("Average time of client connection: " + timeOfAllConnections / NUMBER_OF_THREADS + "ms");
		
		float endBalance = client.getBalance();
		assertEquals(initialBalance - NUMBER_OF_THREADS, endBalance, 0);
			
	}
}
