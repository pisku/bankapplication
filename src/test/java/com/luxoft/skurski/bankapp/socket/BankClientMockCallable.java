package com.luxoft.skurski.bankapp.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;

import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.socket.RequestProtocol.ProtocolState;

public class BankClientMockCallable extends BankClient implements Callable<Long> {
	private Client client;
	private float withdrawAmount;
	
	BankClientMockCallable(int port, Client client, float withdrawAmount) {
		super(port);
		this.client = client;
		this.withdrawAmount = withdrawAmount;
	}
	
	@Override
	public Long call() {
		long start = System.currentTimeMillis();
		
		try {
			// 1. creating a socket to connect to the server
			requestSocket = new Socket(SERVER, SERVER_PORT);
			System.out.println("Connected to localhost in port 2004");
			// 2. get Input and Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			// 3: Communicating with the server
			do {
				try {
					clientRequest = (ClientRequest) in.readObject();
					processInput();
					sendMessage(clientRequest);	
					
				} catch (ClassNotFoundException classNot) {
					System.err.println("data received in unknown format");
				}
			} while (!endTransaction);
		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			// 4: Closing connection
			try {
				in.close();
				out.close();
				requestSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
		
		long end = System.currentTimeMillis();
		
		return (long) end - start;
	}
	
	void processInput() {
		if (clientRequest.getProtocolState() == ProtocolState.LOGIN) {
			System.out.println(clientRequest.getMessage());
			clientRequest = new ClientRequest(client.getName()); // getFullNameFromInput()
			clientRequest.setMessage("username to login");
		} else if (clientRequest.getProtocolState() == ProtocolState.OPERATION) {
			System.out.println(clientRequest.getMessage());
			clientRequest.setActiveAccountId(client.getActiveAccount().getId());
			//withdraw operation
			clientRequest.setOperation("withdraw");
			clientRequest.setWithdraw(withdrawAmount);
			clientRequest.setMessage("account and operation data");
		} else if (clientRequest.getProtocolState() == ProtocolState.FINISHED) {
			System.out.println(clientRequest.getMessage());
			System.out.println(clientRequest.getBalance() + " zl");
			clientRequest.setMessage("user finished his operation");
		} else if (clientRequest.getProtocolState() == ProtocolState.CLOSING) {
			System.out.println(clientRequest.getMessage());
			clientRequest.setMessage("closing connection");
			endTransaction = true;
		}
	}

}
