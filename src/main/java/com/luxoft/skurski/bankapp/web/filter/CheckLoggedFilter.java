package com.luxoft.skurski.bankapp.web.filter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CheckLoggedFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(CheckLoggedFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOG.info("Start filter.");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest) request).getSession();
        String clientName = (String) session.getAttribute("clientName");
        String path = ((HttpServletRequest) request).getRequestURI();
        LOG.debug("Uri path: {}", path);

        if (path.startsWith("/login")) {
            chain.doFilter(request, response);
        }
        else if (path.equals("/")) {
            chain.doFilter(request, response);
        }
        else if (clientName != null) {
            chain.doFilter(request, response);
        }
        else if (path.startsWith("/resources")) {
            chain.doFilter(request, response);
        }
        else {
            ((HttpServletResponse) response).sendRedirect("/login");
        }
    }

    @Override
    public void destroy() {
        LOG.info("Destroy filter.");
    }
}
