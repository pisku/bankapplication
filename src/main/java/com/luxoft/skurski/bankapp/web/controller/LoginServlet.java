package com.luxoft.skurski.bankapp.web.controller;

import com.luxoft.skurski.bankapp.dao.*;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(LoginServlet.class.getName());
    private Bank bank = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        bank = new Bank("PS Bank");
        bank.setId(1);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("view/login.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        final String clientName = request.getParameter("clientName");

        Client client = null;
        try {
            if (clientName == null)
                throw new ClientNotExistsException();

            ClientDAO clientDAO = new ClientDAOImpl();
            client = clientDAO.findClientByName(bank, clientName);
            LOG.debug("Client with name find in the bank: {}", client.getName());

        } catch (ClientNotExistsException e) {
            LOG.warn("Client not found!");
            request.setAttribute("exception", e.printMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("view/login.jsp");
            dispatcher.forward(request, response);
            return;
        } catch (DAOException e) {
            e.printStackTrace();
        }

        request.getSession().setAttribute("clientName", clientName);
        request.getSession().setAttribute("client", client);
        LOG.info("Client {} logged into ATM.", clientName);

        response.sendRedirect("/atm");
    }
}