package com.luxoft.skurski.bankapp.web.controller;

import com.luxoft.skurski.bankapp.dao.ClientDAO;
import com.luxoft.skurski.bankapp.dao.ClientDAOImpl;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.util.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AtmServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(AtmServlet.class.getName());

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                    throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("view/atm.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
                    throws ServletException, IOException {

        String operationType = request.getParameter("operationType");
        Client client = (Client) request.getSession().getAttribute("client");

        ClientDAO clientDAO = new ClientDAOImpl();
        if (makeOperation(request, client, operationType)) {
            try {
                clientDAO.save(client);
            } catch (DAOException e) {
                LOG.error("Database error");
                request.setAttribute("exception", e.printMessage());
                e.printStackTrace();
            }
        }

        Float balanceAmount = client.getBalance();
        request.setAttribute("balance", balanceAmount);

        RequestDispatcher dispatcher = request.getRequestDispatcher("view/atm.jsp");
        dispatcher.forward(request, response);
    }

    private boolean makeOperation(HttpServletRequest request,
                                  Client client, String operation) {

        try {
            if (operation.equals("withdraw")) {
                try {
                    validateField(request.getParameter("withdraw"));
                    Float withdrawAmount = Float.valueOf(request.getParameter("withdraw"));
                    client.getActiveAccount().withdraw(withdrawAmount);
                } catch (NotEnoughFundsException e) {
                    LOG.warn("Not enough funds in client account: {}", client.getName());
                    request.setAttribute("exception", e.printMessage());
                }
                return true;
            } else if (operation.equals("deposit")) {

                validateField(request.getParameter("deposit"));
                Float depositAmount = Float.valueOf(request.getParameter("deposit"));
                client.getActiveAccount().deposit(depositAmount);

                return true;
            }
        } catch (IllegalArgumentException e) {
            LOG.warn("Invalid number for withdraw, client name: {}", client.getName());
            request.setAttribute("exception", e.getMessage());
        }
        return false;
    }

    private void validateField(String field) throws IllegalArgumentException {
        if (!Validation.checkIsPositiveNumber(field)) {
            LOG.debug("Invalid number - exception is thrown");
            throw new IllegalArgumentException("Please provide valid number!");
        }
    }

}

