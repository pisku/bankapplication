package com.luxoft.skurski.bankapp.testfeed;

import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.model.SavingAccount;
import com.luxoft.skurski.bankapp.service.BankService;
import com.luxoft.skurski.bankapp.service.BankServiceImpl;

public class TestSerialization {

	public static void run() {
		Client client = new Client("Skurski", Gender.MALE, 5432f, "skurski@gmail.com",
				"294-543-234", "Cracov");
		client.addAccount(new CheckingAccount(300, 100));
		client.addAccount(new SavingAccount(555));

		BankService bankService = new BankServiceImpl();
		bankService.saveClient(client);
		Client recoveredClient = bankService.loadClient();
		
		System.out.println("Serialization test:");
		System.out.println(client);
		System.out.println(recoveredClient);
		if (client.equals(recoveredClient)) {
			System.out.println("Serialization: OK");
		} else {
			System.out.println("Serialization: Not OK");
		}
	}
	
	public static void main(String[] args) {
		run();
	}
}
