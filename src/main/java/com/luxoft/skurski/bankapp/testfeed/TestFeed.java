package com.luxoft.skurski.bankapp.testfeed;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TestFeed {

	public static void createFolderWithFeedFiles() {
		File catalog = new File("src/main/resources/feed");
		catalog.getParentFile().mkdirs();
		catalog.mkdir();
		File database1 = new File("src/main/resources/feed/database1.txt");
		File database2 = new File("src/main/resources/feed/database2.txt");
		try {
			database1.createNewFile();
			database2.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try (BufferedWriter writer1 = new BufferedWriter(new FileWriter(database1));
				BufferedWriter writer2 = new BufferedWriter(new FileWriter(database2))) {
			
			writer1.write("accounttype=c;balance=100;overdraft=50;name=John Kennedy;gender=m;\n");
			writer1.write("accounttype=s;balance=10000;overdraft=50;name=Eve Something;gender=f;\n");
			writer1.write("accounttype=c;balance=2500;overdraft=50;name=Smith John;gender=m;\n");
			writer1.write("accounttype=c;balance=2500;overdraft=50;name=Piotr Skurski;gender=m;\n");
			
			writer2.write("accounttype=c;balance=600;overdraft=50;name=Tester App;gender=m;\n");
			writer2.write("accounttype=s;balance=999;overdraft=50;name=Another Lastname;gender=f;\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}