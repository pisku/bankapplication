package com.luxoft.skurski.bankapp.test.service;

import com.luxoft.skurski.bankapp.dao.NoDB;

import java.lang.reflect.Field;


public class TestService {
    /**
     * This method should analyze the fields o1 and o2.
     * It should compare all the fields with the help of equals,
     * except for those fields that are marked with an annotation
     *  @NoDB
     * And return true, if all the fields are equal.
     * Also it should be able to compare the collections.
     */
    public static boolean isEquals (Object o1, Object o2) throws IllegalAccessException {

        Class clazzObj1 = o1.getClass();
        Class clazzObj2 = o2.getClass();

        Field[] fieldsObj1 = clazzObj1.getDeclaredFields();
        Field[] fieldsObj2 = clazzObj2.getDeclaredFields();

        if (fieldsObj1.length != fieldsObj2.length)
            return false;

        return compareFields(fieldsObj1, fieldsObj2, o1, o2);
    }

    private static boolean compareFields(Field[] fields1, Field[] fields2,
                                         Object o1, Object o2) throws IllegalAccessException {

        for (int i=0; i<fields1.length; i++) {
            if (!fields1[i].isAnnotationPresent(NoDB.class)
                    && !fields2[i].isAnnotationPresent(NoDB.class)) {

                fields1[i].setAccessible(true);
                fields2[i].setAccessible(true);

                if (!fields1[i].get(o1).equals(fields2[i].get(o2))) {
                    return false;
                }
            }
        }

        return true;
    }

}