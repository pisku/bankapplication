package com.luxoft.skurski.bankapp.command.dao;


import com.luxoft.skurski.bankapp.command.Command;
import com.luxoft.skurski.bankapp.command.UIForm;
import com.luxoft.skurski.bankapp.dao.BankDAO;
import com.luxoft.skurski.bankapp.dao.BankDAOImpl;
import com.luxoft.skurski.bankapp.dao.ClientDAO;
import com.luxoft.skurski.bankapp.dao.ClientDAOImpl;
import com.luxoft.skurski.bankapp.exception.BankNotFoundException;
import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class DBSelectBankCommander implements Command {
    private static final Logger LOG = LoggerFactory.getLogger(DBSelectBankCommander.class.getName());

    @Override
    public void execute() {
        try {
            String name = UIForm.getBankName();

            BankDAO bankDAO = new BankDAOImpl();
            ClientDAO clientDAO = new ClientDAOImpl();
            Bank bank = bankDAO.getBankByName(name);
            List<Client> clients = clientDAO.getAllClients(bank);

            for (Client client: clients) {
                try {
                    bank.addClient(client);
                } catch (ClientExistsException e) {
                    System.out.println(e.printMessage());
                    LOG.debug("{}", e.printMessage());
                }
            }

            BankCommander.setCurrentBank(bank);
            System.out.println("You set active bank: " + bank.getName());

        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("{}", e.getMessage());
        } catch (BankNotFoundException e) {
            System.out.println(e.printMessage());
            LOG.debug("{}", e.printMessage());
        } catch (DAOException e) {
            System.out.println(e.printMessage());
            LOG.error("{}", e.printMessage());
        }
    }

    @Override
    public void printCommandInfo() {
        System.out.print("Select bank from DB.");
    }
}
