package com.luxoft.skurski.bankapp.command;

import java.io.IOException;
import java.util.Set;

import com.luxoft.skurski.bankapp.dao.ClientDAO;
import com.luxoft.skurski.bankapp.dao.ClientDAOImpl;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransferCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(TransferCommand.class.getName());

	@Override
	public void execute() {
		
		if (BankCommander.getCurrentClient() != null &&
				BankCommander.getCurrentClient().getActiveAccount() != null) {
			try {
				String name = UIForm.getFullName();
				Client client = BankCommander.getCurrentBank().findClient(name);
				Set<Account> accounts = client.getAccounts();
				int accountId = Integer.parseInt(UIForm.getActiveAccountId(accounts));
				float transfer = Float.parseFloat(UIForm.getAmount("transfer"));
				
				Account account = client.searchAccount(accountId);
				if (account != null) {
					BankCommander.getCurrentClient().getActiveAccount().withdraw(transfer);
					account.deposit(transfer);

					ClientDAO clientDAO = new ClientDAOImpl();
					clientDAO.save(BankCommander.getCurrentClient());
					clientDAO.save(client);

					System.out.println("Transfer has been made.");
				} else {
					System.out.println("No account has been choosen");
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error("{}",e.getMessage());
			} catch (ClientNotExistsException cnee) {
				System.out.println(cnee.printMessage());
				LOG.debug("{}",cnee.printMessage());
			} catch (NotEnoughFundsException nefe) {
				System.out.println(nefe.printMessage());
				LOG.debug("{}",nefe.printMessage());
			} catch (DAOException e) {
                System.out.println(e.printMessage());
				LOG.error("{}",e.printMessage());
            }
        } else {
			System.out.println("No active client or no active account.");
		}
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Transfer funds to another account.");
	}

}
