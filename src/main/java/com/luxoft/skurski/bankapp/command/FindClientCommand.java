package com.luxoft.skurski.bankapp.command;

import java.io.IOException;

import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.service.BankCommander;
import com.luxoft.skurski.bankapp.service.BankServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FindClientCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(FindClientCommand.class.getName());

	@Override
	public void execute() {

		try {
			String name = UIForm.getFullName();
			
//			BankCommander.currentClient = BankCommander.currentBank.findClient(name);
			BankCommander.setCurrentClient(new BankServiceImpl()
					.getClient(BankCommander.getCurrentBank(), name));
			System.out.println("Active client: " + name);
		} catch (IOException e) {
			e.printStackTrace();
			LOG.error("{}",e.getMessage());
		} catch (ClientNotExistsException cnee) {
			System.out.println(cnee.printMessage() + "\n");
			LOG.debug("{}", cnee.printMessage());
		}
		
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Find client. (deprecated - use select from db)");
	}

}
