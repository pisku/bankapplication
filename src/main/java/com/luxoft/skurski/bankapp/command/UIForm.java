package com.luxoft.skurski.bankapp.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Set;

import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.util.Validation;

public class UIForm {
	private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public static String getFullName() throws IOException {
		System.out.println("Enter full name: ");
		String name = reader.readLine();
		if (!Validation.checkIsFullName(name)) {
			System.out.println("Please provide firstname and surname with "
					+ "first capital letter!");
			name = getFullName();
		}
		return name;
	}

	public static String getBankName() throws IOException {
        System.out.println("Enter bank name: ");
        String name = reader.readLine();
        if (!Validation.checkAreWords(name)) {
            System.out.println("Please provide name with "
                    + "first capital letter!");
            name = getBankName();
        }
        return name;
    }
	
	public static String getEmail() throws IOException {
		System.out.println("Enter email address: ");
		String email = reader.readLine();
		if (!Validation.checkIsEmail(email)) {
			System.out.println("Invalid format of email!");
			email = getEmail();
		}
		return email;
	}
	
	public static Gender getGender() throws IOException {
		System.out.println("Choose gender: ");
		System.out.println("0) Male ");
		System.out.println("1) Female");
		String genderStr = reader.readLine();
		if (!Validation.checkIsExpectedNumber(genderStr, 1)) {
			System.out.println("Invalid number selected!");
			getGender();
		}
		Gender gender = null;
		switch (genderStr) {
			case "0": gender = Gender.MALE; break;
			case "1": gender = Gender.FEMALE; break;
		}
		return gender;
	}
	
	public static String getPhone() throws IOException {
		System.out.println("Enter phone number: ");
		String phone = reader.readLine();
		if (!Validation.checkIsPhone(phone)) {
			System.out.println("Invalid phone number!");
			phone = getPhone();
		}
		return phone;
	}
	
	public static String getOverdraft() throws IOException {
		System.out.println("Enter initial overdraft: ");
		String overdraftStr = reader.readLine();
		if (!Validation.checkIsPositiveNumber(overdraftStr)) {
			System.out.println("Invalid format of overdraft!");
			overdraftStr = getOverdraft();
		}
		return overdraftStr;
	}
	
	public static String getAccountType() throws IOException {
		System.out.println("Choose account type: ");
		System.out.println("0) Saving account ");
		System.out.println("1) Checking account");
		String accountStr = reader.readLine();
		if (!Validation.checkIsExpectedNumber(accountStr, 1)) {
			System.out.println("Invalid number selected!");
			accountStr = getAccountType();
		}
		return accountStr;
	}
	
	public static String getAmount(String actionName) throws IOException {
		System.out.println("Operation: " + actionName + ", enter amount: ");
		String amountStr = reader.readLine();
		if (!Validation.checkIsPositiveNumber(amountStr)) {
			System.out.println("Invalid number format!");
			amountStr = getAmount(actionName);
		}
		return amountStr;
	}
	
	public static String getActiveAccountId(Set<Account> accounts) throws IOException {
		System.out.println("Choose account, enter ID: ");
		int idArr[] = new int[accounts.size()];
		Iterator<Account> iter = accounts.iterator();
		int i = 0;
		while (iter.hasNext()) {
			Account account = iter.next();
			System.out.println(account.printReport());
			idArr[i++] = account.getId();
		}

		String id = reader.readLine();
		if (!Validation.checkIsNumberFromArray(id, idArr)) {
			System.out.println("Invalid number selected!");
			id = getActiveAccountId(accounts);
		}
		return id;
	}
	
	public static String chooseBalanceOrWithdraw() throws IOException {
		System.out.println("Choose type of operation:");
		System.out.println("0) Check your balance.");
		System.out.println("1) Withdraw money.");
		System.out.print("Select a number: ");
		String answer = reader.readLine();
		if (!Validation.checkIsExpectedNumber(answer, 1)) {
			System.out.println("Invalid number selected!");
			answer = chooseBalanceOrWithdraw();
		}
		
		switch (answer) {
			case "0": return "balance";
			case "1": return "withdraw";
			default: return "balance";
		}
	}
}
