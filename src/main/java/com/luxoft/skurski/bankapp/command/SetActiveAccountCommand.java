package com.luxoft.skurski.bankapp.command;

import java.io.IOException;
import java.util.Set;

import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetActiveAccountCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(SetActiveAccountCommand.class.getName());

	@Override
	public void execute() {
		
		if (BankCommander.getCurrentClient() != null &&
				BankCommander.getCurrentClient().getActiveAccount() != null) {
			try {
				Set<Account> accounts = BankCommander.getCurrentClient().getAccounts();
				int accountId = Integer.parseInt(UIForm.getActiveAccountId(accounts));
				Account choosenAccount = BankCommander.getCurrentClient().searchAccount(accountId);
				if (choosenAccount != null) {
					BankCommander.getCurrentClient().setActiveAccount(choosenAccount);
					System.out.println("Choosen account: " + choosenAccount.printReport());
				} else {
					System.out.println("No active account.");
				}
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error("{}",e.getMessage());
			}
		} else {
			System.out.println("No active client or no accounts.");
			LOG.debug("No active client or no accounts.");
		}
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Set active account.");
	}

}
