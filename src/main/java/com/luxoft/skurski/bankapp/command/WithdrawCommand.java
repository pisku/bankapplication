package com.luxoft.skurski.bankapp.command;

import java.io.IOException;

import com.luxoft.skurski.bankapp.dao.ClientDAO;
import com.luxoft.skurski.bankapp.dao.ClientDAOImpl;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WithdrawCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(WithdrawCommand.class.getName());

	@Override
	public void execute() {
		
		if (BankCommander.getCurrentClient() != null &&
				BankCommander.getCurrentClient().getActiveAccount() != null) {
			try {
				float withdraw = Float.parseFloat(UIForm.getAmount("withdraw"));
				
				BankCommander.getCurrentClient().getActiveAccount().withdraw(withdraw);
				ClientDAO clientDAO = new ClientDAOImpl();
				clientDAO.save(BankCommander.getCurrentClient());

				System.out.println("Withdraw for " + withdraw + " has been made.");
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error("{}",e.getMessage());
			} catch (NotEnoughFundsException nefe) {
				System.out.println(nefe.printMessage());
				LOG.debug("{}",nefe.printMessage());
			} catch (DAOException e) {
                System.out.println(e.printMessage());
				LOG.debug("{}",e.printMessage());
            }
        } else {
			System.out.println("No active client or no active account.");
			LOG.debug("No active client or no active account");
		}
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Withdraw funds.");
	}

}
