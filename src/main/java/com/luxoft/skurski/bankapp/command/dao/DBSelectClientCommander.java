package com.luxoft.skurski.bankapp.command.dao;

import com.luxoft.skurski.bankapp.command.Command;
import com.luxoft.skurski.bankapp.command.UIForm;
import com.luxoft.skurski.bankapp.dao.*;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.util.List;


public class DBSelectClientCommander implements Command {
    private static final Logger LOG = LoggerFactory.getLogger(DBSelectClientCommander.class.getName());

    @Override
    public void execute() {
        if (BankCommander.getCurrentBank() != null) {
            try {
                String name = UIForm.getFullName();

                ClientDAO clientDAO = new ClientDAOImpl();
                AccountDAO accountDAO = new AccountDAOImpl();

                Client client = clientDAO.findClientByName(BankCommander.getCurrentBank(), name);

                List<Account> accounts = accountDAO.getClientAccounts(client.getId());
                if (!accounts.isEmpty()) {
                    client.setAccounts(accounts);
                    client.setDefaultActiveAccount();
                }

                BankCommander.setCurrentClient(client);
                System.out.println("You set active client: " + client.getName());

            } catch (IOException e) {
                e.printStackTrace();
                LOG.error("{}", e.getMessage());
            } catch (DAOException e) {
                System.out.println(e.printMessage());
                LOG.error("{}", e.printMessage());
            } catch (ClientNotExistsException e) {
                System.out.println(e.printMessage());
                LOG.debug("{}", e.printMessage());
            }
        } else {
            System.out.println("No active bank! First select the bank.");
            LOG.debug("No active bank! First select the bank.");
        }
    }

    @Override
    public void printCommandInfo() {
        System.out.print("Select client from DB.");
    }
}
