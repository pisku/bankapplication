package com.luxoft.skurski.bankapp.command;

import java.io.IOException;

import com.luxoft.skurski.bankapp.dao.ClientDAO;
import com.luxoft.skurski.bankapp.dao.ClientDAOImpl;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DepositCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(DepositCommand.class.getName());

	@Override
	public void execute() {
		
		if (BankCommander.getCurrentClient() != null &&
				BankCommander.getCurrentClient().getActiveAccount() != null) {
			try {
				float deposit = Float.parseFloat(UIForm.getAmount("deposit"));
				
				BankCommander.getCurrentClient().getActiveAccount().deposit(deposit);
                ClientDAO clientDAO = new ClientDAOImpl();
                clientDAO.save(BankCommander.getCurrentClient());

				System.out.println("Deposit for " + deposit + " has been made.");
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error("{}", e.getMessage());
			} catch (DAOException e) {
                e.printStackTrace();
				LOG.error("{}", e.printMessage());
            }
        } else {
			System.out.println("No active account.");
			LOG.debug("No active account.");
		}
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Deposit funds.");
	}

}
