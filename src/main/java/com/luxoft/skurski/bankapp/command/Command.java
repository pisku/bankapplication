package com.luxoft.skurski.bankapp.command;

public interface Command {
	void execute();
	void printCommandInfo();
}
