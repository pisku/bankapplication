package com.luxoft.skurski.bankapp.command.dao;

import com.luxoft.skurski.bankapp.command.Command;
import com.luxoft.skurski.bankapp.command.FindClientCommand;
import com.luxoft.skurski.bankapp.dao.*;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DBRemoveClientCommander implements Command {
    private static final Logger LOG = LoggerFactory.getLogger(DBRemoveClientCommander.class.getName());

    @Override
    public void execute() {
        if (BankCommander.getCurrentClient() != null) {

            try {
                ClientDAO clientDAO = new ClientDAOImpl();
                Client client = BankCommander.getCurrentClient();
                clientDAO.remove(client);

                Bank bank = BankCommander.getCurrentBank();
                bank.removeClient(client);

                BankCommander.setCurrentClient(null);
                System.out.println("You remove active client");

            } catch (DAOException e) {
                System.out.println(e.printMessage());
                LOG.error("{}",e.printMessage());
            }

        } else {
            System.out.println("No active client! First select the client.");
            LOG.debug("No active client! First select the client.");
        }
    }

    @Override
    public void printCommandInfo() {
        System.out.print("Remove client from DB.");
    }
}
