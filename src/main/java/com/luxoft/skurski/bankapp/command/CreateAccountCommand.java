package com.luxoft.skurski.bankapp.command;

import java.io.IOException;

import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.SavingAccount;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateAccountCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(CreateAccountCommand.class.getName());

	@Override
	public void execute() {
		
		if (BankCommander.getCurrentClient() != null) {
			try {
				int accountType = Integer.parseInt(UIForm.getAccountType());
				float balance = Float.parseFloat(UIForm.getAmount("set initial balance"));
				
				Account account = null;
				switch (accountType) {
					case 0: account = new SavingAccount(balance); break;
					case 1: account = new CheckingAccount(balance); break;
				}
				
				BankCommander.getCurrentClient().addAccount(account);
				System.out.println("Account has been added.");
			} catch (IOException e) {
				e.printStackTrace();
				LOG.error("{}", e.getMessage());
			}
		} else {
			System.out.println("No active client.");
            LOG.debug("No active client");
		}
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Create client account.");
	}

}
