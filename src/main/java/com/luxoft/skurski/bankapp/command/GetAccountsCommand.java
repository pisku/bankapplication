package com.luxoft.skurski.bankapp.command;

import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetAccountsCommand implements Command {
	private static final Logger LOG = LoggerFactory.getLogger(GetAccountsCommand.class.getName());

	@Override
	public void execute() {
		if (BankCommander.getCurrentClient() != null) {
			if (BankCommander.getCurrentClient().getActiveAccount() != null) {
				System.out.println(BankCommander.getCurrentClient().printReport());
			} else {
				System.out.println("No accounts.");
				LOG.debug("No accounts.");
			}
		} else {
			System.out.println("No active client.");
			LOG.debug("No active client.");
		}
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Print client accounts.");
	}

}
