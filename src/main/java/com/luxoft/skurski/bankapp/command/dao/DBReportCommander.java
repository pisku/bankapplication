package com.luxoft.skurski.bankapp.command.dao;

import com.luxoft.skurski.bankapp.command.Command;
import com.luxoft.skurski.bankapp.dao.BankDAO;
import com.luxoft.skurski.bankapp.dao.BankDAOImpl;
import com.luxoft.skurski.bankapp.model.BankInfo;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DBReportCommander implements Command {
    private static final Logger LOG = LoggerFactory.getLogger(DBReportCommander.class.getName());

    @Override
    public void execute() {
        if (BankCommander.getCurrentBank() != null) {

            BankDAO bankDAO = new BankDAOImpl();
            BankInfo bankInfo = bankDAO.getBankInfo(BankCommander.getCurrentBank());
            System.out.println(bankInfo.printInfo());

        } else {
            System.out.println("No active bank! First select the bank.");
            LOG.debug("No active bank! First select the bank.");
        }
    }

    @Override
    public void printCommandInfo() {
        System.out.print("Print active bank info");
    }
}
