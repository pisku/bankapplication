package com.luxoft.skurski.bankapp.command;

import java.io.IOException;

import com.luxoft.skurski.bankapp.dao.ClientDAO;
import com.luxoft.skurski.bankapp.dao.ClientDAOImpl;
import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.service.BankApplication;
import com.luxoft.skurski.bankapp.service.BankCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddClientCommand implements Command {
    private static final Logger LOG = LoggerFactory.getLogger(AddClientCommand.class.getName());

	@Override
	public void execute() {
        if (BankCommander.getCurrentBank() != null) {
            try {
                String name = UIForm.getFullName();
                String email = UIForm.getEmail();
                String phone = UIForm.getPhone();
                Gender gender = UIForm.getGender();
                float initialOverdraft = Float.parseFloat(UIForm.getOverdraft());

                Client client = new Client(name, gender, initialOverdraft, email, phone);
                client.setBankId(BankCommander.getCurrentBank().getId());
                BankCommander.getCurrentBank().addClient(client);
                BankCommander.setCurrentClient(client);

                ClientDAO clientDAO = new ClientDAOImpl();
                clientDAO.save(BankCommander.getCurrentClient());

            } catch (IOException e) {
                e.printStackTrace();
                LOG.error("{}", e.getMessage());
            } catch (ClientExistsException cee) {
                System.out.println(cee.printMessage());
                LOG.debug("ClientExistsException: {}", cee.printMessage());
            } catch (DAOException e) {
                e.printStackTrace();
                LOG.error("{}", e.printMessage());
            }
        } else {
            System.out.println("No active bank! Choose active bank first.");
            LOG.debug("No active bank.");
        }
	}

	@Override
	public void printCommandInfo() {
		System.out.print("Add new client.");
	}
}
