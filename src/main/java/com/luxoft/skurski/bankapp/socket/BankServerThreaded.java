package com.luxoft.skurski.bankapp.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.service.BankFeedService;
import com.luxoft.skurski.bankapp.testfeed.TestFeed;

public class BankServerThreaded implements Runnable {
	private ServerSocket serverSocket;
	private ExecutorService pool;
	private final int POOL_SIZE = 20;
	private final int PORT;
	private static AtomicInteger connectionCounter = new AtomicInteger(0);
	private BankServerMonitor monitor = new BankServerMonitor();
	private Bank bank;
	private volatile boolean running = true;

	BankServerThreaded(Bank bank, int port) {
		this.bank = bank;
		this.PORT = port;

		TestFeed.createFolderWithFeedFiles();
		new BankFeedService(bank).loadFeed("src/main/resources/feed");
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public static void decrementConnectionCounter() {
		BankServerThreaded.connectionCounter.decrementAndGet();
	}

	public static void incrementConnectionCounter(
			AtomicInteger connectionCounter) {
		BankServerThreaded.connectionCounter.incrementAndGet();
	}

	public static int getConnectionCounter() {
		return BankServerThreaded.connectionCounter.get();
	}

	@Override
	public void run() {
		try {
			serverSocket = new ServerSocket(PORT);
			// pool = Executors.newSingleThreadExecutor();
			pool = Executors.newFixedThreadPool(POOL_SIZE);
			System.out.println("Waiting for connection");
			
			pool.execute(monitor);
			while (running) {
				pool.execute(new ServerThread(serverSocket.accept(), bank));
				connectionCounter.incrementAndGet();
			}
		} catch (IOException ex) {
			pool.shutdown();
		} finally {
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

//	 public static void main(String[] args) {
//		 BankServerThreaded bankServer = new BankServerThreaded(new Bank(), 2004);
//		 bankServer.run();
//	 }
}
