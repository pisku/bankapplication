package com.luxoft.skurski.bankapp.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.service.BankService;
import com.luxoft.skurski.bankapp.service.BankServiceImpl;
import com.luxoft.skurski.bankapp.socket.RequestProtocol.ProtocolState;

public class ServerThread implements Runnable {
	private static final Logger LOG = LoggerFactory.getLogger(BankServer.class);

	private Socket connection = null;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private ClientRequest clientRequest;
	private Bank bank;
	private BankService bankService = new BankServiceImpl();
	private boolean endTransaction = false;
	
	ServerThread(Socket clientSocket, Bank bank) {
		connection = clientSocket;
		this.bank = bank;
	}

	@Override
	public void run() {
		try {
			System.out.println("Connection received from " + connection.getInetAddress().getHostName());
			// 3. get Input and Output streams
			out = new ObjectOutputStream(connection.getOutputStream());
			out.flush();
			in = new ObjectInputStream(connection.getInputStream());
			// 4. The two parts communicate via the input and output streams
			RequestProtocol protocol = new RequestProtocol();
			clientRequest = protocol.processInput(clientRequest, bank, bankService);
			do {
				try {
		            if (clientRequest.getProtocolState() == ProtocolState.CLOSING) {
		            	endTransaction = true;
		            }
	            	sendMessage(clientRequest);
		            clientRequest = protocol.processInput((ClientRequest) in.readObject(), bank, bankService);
		            LOG.debug("Server > received > {}", clientRequest);
				} catch (ClassNotFoundException e) {
					System.err.println("data received in unknown format");
				}

			} while (!endTransaction);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			// 4: Closing connection
			try {
				in.close();
				out.close();
				connection.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
			BankServerThreaded.decrementConnectionCounter();
		}
	}

	void sendMessage(final ClientRequest obj) {
		try {
			out.writeObject(obj);
			out.flush();
			System.out.println("server > send > " + obj.getMessage());
			LOG.debug("server > send > {}", obj);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}
}
