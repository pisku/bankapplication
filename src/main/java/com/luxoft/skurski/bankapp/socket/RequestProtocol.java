package com.luxoft.skurski.bankapp.socket;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.service.BankService;


public class RequestProtocol {
	private static final Logger LOG = LoggerFactory.getLogger(RequestProtocol.class.getName());
    
    protected enum ProtocolState { WAITING, LOGIN, OPERATION, FINISHED, CLOSING };
    private ProtocolState state = ProtocolState.WAITING;
 
    public ClientRequest processInput(ClientRequest request, Bank bank, BankService bankService) {
        if (state == ProtocolState.WAITING) {
        	LOG.debug("protocol > WAITING state");
        	request = new ClientRequest();
        	request.setMessage("Login to ATM using your full name");
        	request.setProtocolState(ProtocolState.LOGIN);
            state = ProtocolState.LOGIN;
        } else if (state == ProtocolState.LOGIN) {
        	LOG.debug("protocol > LOGIN state");
        	Client client = null;
			try {
				client = bankService.getClient(bank, request.getName());
	        	Set<Account> accounts = client.getAccounts();
	        	request.setAccounts(accounts);
	        	request.setMessage("Choose account and operation");
	        	request.setProtocolState(ProtocolState.OPERATION);
	        	state = ProtocolState.OPERATION;
			} catch (ClientNotExistsException e) {
				System.out.println("server > " + e.printMessage());
				request.setMessage("server > sent > " + e.printMessage() + "\nLogin to ATM using your full name");
				request.setProtocolState(ProtocolState.LOGIN);
			}
        } else if (state == ProtocolState.OPERATION) {
        	LOG.debug("protocol > OPERATION state");

			Client client = null;
			try {
				client = bankService.getClient(bank, request.getName());
				Account searchedAccount = client.searchAccount(request.getActiveAccountId());
				
				if (request.getOperation().equals("withdraw")) {
					searchedAccount.withdraw(request.getWithdraw());
				}
				
				request.setBalance(searchedAccount.getBalance());
	        	request.setMessage("Account balance after operation: " + request.getOperation());
	        	request.setProtocolState(ProtocolState.FINISHED);
	        	state = ProtocolState.FINISHED;
			} catch (ClientNotExistsException e) {
				LOG.debug("server > send > Client Not Exists Exception");
			} catch (NotEnoughFundsException e) {
				System.out.println("server > " + e.printMessage());
				request.setMessage("server > sent > Not Enough Funds Exception: " + e.printMessage());
				request.setProtocolState(ProtocolState.OPERATION);
			}
        } else if (state == ProtocolState.FINISHED) {
        	LOG.debug("protocol > FINISHED state");
        	request.setMessage("ATM operation status: OK (finished)");
        	request.setProtocolState(ProtocolState.CLOSING);
        	state = ProtocolState.CLOSING;
        } else if (state == ProtocolState.CLOSING) {
        	LOG.debug("protocol > CLOSING state");
        	state = ProtocolState.WAITING;
        }
        
        return request;
    }
}