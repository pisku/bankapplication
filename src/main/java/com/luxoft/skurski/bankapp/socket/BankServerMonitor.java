package com.luxoft.skurski.bankapp.socket;

public class BankServerMonitor implements Runnable {

	@Override
	public void run() {
		while (true) {
			System.out.println("Number of clients connected to Bank Server: " +
							BankServerThreaded.getConnectionCounter());
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	
}
