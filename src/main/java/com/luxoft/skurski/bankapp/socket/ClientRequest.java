package com.luxoft.skurski.bankapp.socket;

import java.io.Serializable;
import java.util.Set;

import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.socket.RequestProtocol.ProtocolState;

class ClientRequest implements Serializable {
	private static final long serialVersionUID = -770544052188337908L;
	
	private String name;
	private String operation;
	private Set<Account> accounts;
	private int activeAccountId;
	private ProtocolState protocolState = ProtocolState.WAITING;
	private String message;
	private float balance;
	private float withdraw;
	
	ClientRequest() {}
	
	ClientRequest(String name) {
		this.name = name;
	}
	
	ClientRequest(String name, String operation) {
		this(name);
		this.operation = operation;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public float getWithdraw() {
		return withdraw;
	}

	public void setWithdraw(float withdraw) {
		this.withdraw = withdraw;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ProtocolState getProtocolState() {
		return protocolState;
	}

	public void setProtocolState(ProtocolState protocolState) {
		this.protocolState = protocolState;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	public int getActiveAccountId() {
		return activeAccountId;
	}

	public void setActiveAccountId(int activeAccount) {
		this.activeAccountId = activeAccount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	@Override
	public String toString() {
		return "ClientRequest [name=" + name + ", operation=" + operation
				+ ", accounts=" + accounts + ", activeAccountId="
				+ activeAccountId + ", protocolState=" + protocolState
				+ ", message=" + message + ", balance=" + balance
				+ ", withdraw=" + withdraw + "]";
	}

}
