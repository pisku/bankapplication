package com.luxoft.skurski.bankapp.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.bankapp.command.UIForm;
import com.luxoft.skurski.bankapp.socket.RequestProtocol.ProtocolState;

public class BankClient implements Runnable {
	protected static final Logger LOG = LoggerFactory.getLogger(BankServer.class);

	protected Socket requestSocket;
	protected ObjectOutputStream out;
	protected ObjectInputStream in;
	protected String message;
	protected ClientRequest clientRequest;
	protected static final String SERVER = "localhost";
	protected final int SERVER_PORT;
	protected boolean endTransaction = false;

	public BankClient(int port) {
		this.SERVER_PORT = port;
	}
	
	@Override
	public void run() {
		try {
			// 1. creating a socket to connect to the server
			requestSocket = new Socket(SERVER, SERVER_PORT);
			System.out.println("Connected to localhost in port 2004");
			// 2. get Input and Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			// 3: Communicating with the server
			do {
				try {
					clientRequest = (ClientRequest) in.readObject();
					LOG.debug("client > received > {}", clientRequest);
					processInput();
					sendMessage(clientRequest);	
					
				} catch (ClassNotFoundException classNot) {
					System.err.println("data received in unknown format");
				}
			} while (!endTransaction);
		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			// 4: Closing connection
			try {
				in.close();
				out.close();
				requestSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	void sendMessage(final ClientRequest obj) {
		try {
			out.writeObject(obj);
			out.flush();
			System.out.println("client > send > " + obj.getMessage());
			LOG.debug("client > send > {}", obj);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}
	
	String getFullNameFromInput() {
		String name = null;
		try {
			name = UIForm.getFullName();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return name;
	}
	
	void chooseAccountAndOperation() {
		String operation = null;
		try {
			clientRequest.setActiveAccountId(Integer.parseInt(UIForm.getActiveAccountId(clientRequest.getAccounts())));
			operation = UIForm.chooseBalanceOrWithdraw();
			if (operation.equals("withdraw")) {
				clientRequest.setWithdraw(Float.parseFloat(UIForm.getAmount("withdraw")));
				
			}
			clientRequest.setOperation(operation);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void processInput() {
		if (clientRequest.getProtocolState() == ProtocolState.LOGIN) {
			System.out.println(clientRequest.getMessage());
			clientRequest = new ClientRequest(getFullNameFromInput());
			clientRequest.setMessage("username to login");
		} else if (clientRequest.getProtocolState() == ProtocolState.OPERATION) {
			System.out.println(clientRequest.getMessage());
			chooseAccountAndOperation();
			clientRequest.setMessage("account and operation data");
		} else if (clientRequest.getProtocolState() == ProtocolState.FINISHED) {
			System.out.println(clientRequest.getMessage());
			System.out.println(clientRequest.getBalance() + " zl");
			clientRequest.setMessage("user finished his operation");
		} else if (clientRequest.getProtocolState() == ProtocolState.CLOSING) {
			System.out.println(clientRequest.getMessage());
			clientRequest.setMessage("closing connection");
			endTransaction = true;
		}
	}

//	public static void main(final String args[]) {
//		BankClient client = new BankClient(2004);
//
//		client.run();
//	}

}