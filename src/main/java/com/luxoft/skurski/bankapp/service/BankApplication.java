package com.luxoft.skurski.bankapp.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Set;

import com.luxoft.skurski.bankapp.dao.FillDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;
import com.luxoft.skurski.bankapp.exception.OverDraftLimitExceededException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;
import com.luxoft.skurski.bankapp.model.SavingAccount;
import com.luxoft.skurski.bankapp.testfeed.TestFeed;

public class BankApplication {
	private static final Logger LOG = LoggerFactory.getLogger(BankApplication.class.getName());

	private Bank bank = new Bank();
	private BankService bankService = new BankServiceImpl();
	private BankFeedService feedService = new BankFeedService();
	private static final String FEED_FOLDER = "src/main/resources/feed";

	public Bank getBank() {
		return bank;
	}
	
	public BankFeedService getFeedService() {
		return feedService;
	}
	
	public BankService getBankService() {
		return bankService;
	}
	
	public void initialize() {
		Client client1 = createClient("John Black", Gender.MALE, new Random(), "black@wp.pl", "111222333", "Cracov");
		Client client2 = createClient("Peter Smith", Gender.MALE, new Random(), "smith@wp.pl", "999888777", "Warsaw");
		Client client3 = createClient("Eve Versace", Gender.FEMALE, new Random(), "versace@wp.pl", "333444555", "Katowice");
		Client client4 = createClient("John Black", Gender.MALE, new Random(), "black@wp.pl", "111222333", "Cracov");
		Client client5 = createClient("Piotr Kowalski", Gender.MALE, new Random(), "peter@wp.pl", "111222333", "Cracov");
		
		try {
			bankService.addClient(bank, client1);
			bankService.addClient(bank, client2);
			bankService.addClient(bank, client3);
			bankService.addClient(bank, client4);
			bankService.addClient(bank, client5);
		} catch (ClientExistsException e) {
//			System.out.println(e.printMessage());
			LOG.warn("Client Exists Exception occurred: {}", e.printMessage());
		}
		
	}
	
	private void printBankReport() {
		System.out.println(bank.printReport());
	}
	
	private void modifyBank() {
		Set<Client> clients = bank.getClients();
		float[] withdraws = {10f, 40f, 20f, 350f, 300f, 500f, 1000f};
		for (float withdraw: withdraws) {
			withdrawOperation(withdraw, clients);
		}
		
		System.out.println("Modifying, deposit 55.55 PLN ...................");
		float deposit = 55.55f;
		for (Client client: clients) {
			for (Account account: client.getAccounts()) {
				account.deposit(deposit);
			}
		}
	}
	
	private void withdrawOperation(float withdraw, Set<Client> clients) {
		System.out.println("Modifying, withdraw " + withdraw + " PLN ...................");
		for (Client client: clients) {
			for (Account account: client.getAccounts()) {
				try {
					account.withdraw(withdraw);
				} catch (OverDraftLimitExceededException odlee) {
//					System.out.println(odlee.printMessage());
					LOG.warn("OverDraft Limit Exceeded Exception occurred: {}", odlee.printMessage());
				} catch (NotEnoughFundsException nefe) {
//					System.out.println(nefe.printMessage());
					LOG.warn("Not Enough Funds Exception occurred: {}", nefe.printMessage());
				}
			}
		}
		printBankReport();
	}
	
	private Client createClient(String name, Gender gender, Random random, 
									String email, String phone, String city) {
		float balance = random.nextFloat() * 1000 + 100;
		float overdraft = random.nextFloat() * 1000 + 400;
		
		Client client = new Client(name, gender, overdraft, email, phone, city);
		Account checkingAccount = new CheckingAccount(balance);
		Account savingAccount = new SavingAccount(balance);
		
		bankService.addAccount(client, checkingAccount);
		bankService.addAccount(client, savingAccount);
		bankService.setActiveAccount(client, checkingAccount);
//		bankService.setActiveAccount(client, savingAccount);
		
		return client;
	}

    public static void main(String[] args) {

        if (args.length == 0) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            FillDB fillDB = new FillDB();
            fillDB.init();

            while (BankCommander.isRunning()) {
                BankCommander.showMenu();
                try {
                    String commandString = reader.readLine();
                    BankCommander.getCommandMap().get(commandString.trim()).execute();
                } catch (IOException e) {
                    LOG.error("IOException occured: {}", e.getMessage());
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    System.out.println("Invalid number selected!");
                    LOG.info("Null Pointer Exception occurred: Wrong number selected.");
                }
            }

            fillDB.clean();

        } else if (args.length > 0) {
            BankApplication bankApp = new BankApplication();
            System.out.println("------------ Initializing ------------------");
            bankApp.initialize();
            System.out.println();

            if (args[0].equals(("demo"))) {
                bankApp.printBankReport();
                bankApp.modifyBank();
                bankApp.printBankReport();
                System.out.println("------ AFTER DATA FEED -------------------------------------------");
                bankApp.getFeedService().setBank(bankApp.getBank());
                TestFeed.createFolderWithFeedFiles();
                bankApp.getFeedService().loadFeed(FEED_FOLDER);
                bankApp.printBankReport();
            }
            else if (args[0].equals("report")) {
                System.out.println("---------- BANK REPORT -----------");
                BankReport.getAccountsNumber(bankApp.getBank());
                BankReport.getBankCreditSum(bankApp.getBank());
                BankReport.getNumberOfClients(bankApp.getBank());
                BankReport.getClientsSorted(bankApp.getBank());
                BankReport.getClientsByCity(bankApp.getBank());
            }
        }
    }

}

