package com.luxoft.skurski.bankapp.service;

import java.util.HashMap;
import java.util.Map;

import com.luxoft.skurski.bankapp.command.dao.DBRemoveClientCommander;
import com.luxoft.skurski.bankapp.command.dao.DBReportCommander;
import com.luxoft.skurski.bankapp.command.dao.DBSelectBankCommander;
import com.luxoft.skurski.bankapp.command.dao.DBSelectClientCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.luxoft.skurski.bankapp.command.AddClientCommand;
import com.luxoft.skurski.bankapp.command.Command;
import com.luxoft.skurski.bankapp.command.CreateAccountCommand;
import com.luxoft.skurski.bankapp.command.DepositCommand;
import com.luxoft.skurski.bankapp.command.FindClientCommand;
import com.luxoft.skurski.bankapp.command.GetAccountsCommand;
import com.luxoft.skurski.bankapp.command.SetActiveAccountCommand;
import com.luxoft.skurski.bankapp.command.TransferCommand;
import com.luxoft.skurski.bankapp.command.WithdrawCommand;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;

public class BankCommander {
	private static Bank currentBank = null;
	private static Client currentClient = null;
	private static Map<String, Command> commandMap = new HashMap<String, Command>();
    private static boolean isRunning = true;
	
	static Command[] commands = { 
			new AddClientCommand(), 
			new FindClientCommand(),
			new CreateAccountCommand(),
			new GetAccountsCommand(), 
			new SetActiveAccountCommand(),
			new DepositCommand(),
			new TransferCommand(), 
			new WithdrawCommand(),
            new DBSelectBankCommander(),
            new DBSelectClientCommander(),
            new DBRemoveClientCommander(),
            new DBReportCommander(),
			new Command() {
				public void execute() {
					System.out.println("Closing... See you soon!");
					isRunning = false;
				}
				public void printCommandInfo() {
					System.out.println("Exit");
				}
			} 
	};
	
	static {
		for (int i=0; i<commands.length; i++)
			commandMap.put(String.valueOf(i), commands[i]);
	}

    public static Map<String, Command> getCommandMap() {
        return commandMap;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static Bank getCurrentBank() {
        return currentBank;
    }

    public static void setCurrentBank(Bank currentBank) {
        BankCommander.currentBank = currentBank;
    }

    public static Client getCurrentClient() {
        return currentClient;
    }

    public static void setCurrentClient(Client currentClient) {
        BankCommander.currentClient = currentClient;
    }

    public static void registerCommand(String name, Command command) {
		commandMap.put(name, command);
	}
	
	public static void removeCommand(String name) {
		commandMap.remove(name);
	}
	
	public static void showMenu() {
		System.out.println("\n\tBANK MENU:");
        System.out.print("\tActive client: ");
		if (currentClient != null) {
			System.out.print(currentClient.getName());
		} else {
			System.out.print("none, enter: Piotr Skurski");
		}
        System.out.print("\n\tActive bank: ");
        if (currentBank != null) {
            System.out.print(currentBank.getName());
        } else {
            System.out.print("none, enter: PS Bank");
        }
        System.out.println();
		
		for (int i = 0; i < commands.length; i++) {
			System.out.print(i + ") ");
			commandMap.get(String.valueOf(i)).printCommandInfo();
			System.out.println();
		}
		System.out.println("Choose a number: ");
	}

}
