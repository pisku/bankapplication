package com.luxoft.skurski.bankapp.service;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;

public interface BankService {
	public void addClient(Bank bank,Client client) throws ClientExistsException;
	public void removeClient(Bank bank,Client client);
	public void addAccount(Client client, Account account);
	public void setActiveAccount(Client client, Account account);
	public Client getClient(Bank bank, String clientName) throws ClientNotExistsException;
	public void saveClient(Client client);
	public Client loadClient();
}

