package com.luxoft.skurski.bankapp.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.luxoft.skurski.bankapp.model.Bank;


public class BankFeedService {
	private Bank activeBank;
	
	public BankFeedService() {}
	
	public BankFeedService(Bank bank) {
		activeBank = bank;
	}
	
	public void setBank(Bank bank) {
		activeBank = bank;
	}

	public void loadFeed(String folderStr) {
		File folder = new File(folderStr);
		folder.getParentFile().mkdirs();
		folder.mkdir();
		
		for (File fileEntry: folder.listFiles()) {
			if (fileEntry.isFile()) {
				readFile(fileEntry);
			}
		}
	}
	
	private void readFile(File file) {
		
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				Map<String, String> feedMap = new HashMap<String, String>();
				String[] record = line.split(";");
				for (String data: record) {
					String[] property = data.split("=");
					feedMap.put(property[0], property[1]);
				}
				activeBank.parseFeed(feedMap);
			}
		} catch (FileNotFoundException fnfe) {
			System.out.println("File: " + file.getName() + " doesn't exists!");
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
