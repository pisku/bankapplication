package com.luxoft.skurski.bankapp.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.util.Validation;

public class BankServiceImpl implements BankService {
	private static final String CLIENT_FILE = "src/main/resources/client.dat";

	@Override
	public void addClient(Bank bank, Client client) throws ClientExistsException {
		Validation.checkForNull(bank);
		Validation.checkForNull(client);
		
		bank.addClient(client);
	}

	@Override
	public void removeClient(Bank bank, Client client) {
		Validation.checkForNull(bank);
		Validation.checkForNull(client);
		
		bank.removeClient(client);
	}

	@Override
	public void addAccount(Client client, Account account) {
		Validation.checkForNull(client);
		Validation.checkForNull(account);
		
		client.addAccount(account);
	}

	@Override
	public void setActiveAccount(Client client, Account account) {
		Validation.checkForNull(client);
		Validation.checkForNull(account);	
		
		client.setActiveAccount(account);
	}

	@Override
	public Client getClient(Bank bank, String clientName) throws ClientNotExistsException {
		Validation.checkForNull(clientName);
		
		return bank.findClient(clientName);
	}

	@Override
	public void saveClient(Client client) {
		File clientFile = new File(CLIENT_FILE);
		clientFile.getParentFile().mkdirs();
		
		try (ObjectOutputStream ous = new ObjectOutputStream(
				new FileOutputStream(clientFile))) {
			ous.writeObject(client);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Client loadClient() {
		Client client = null;
		try (ObjectInputStream ois = new ObjectInputStream(
				new FileInputStream(CLIENT_FILE))) {
			client = (Client) ois.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return client;
	}

}

