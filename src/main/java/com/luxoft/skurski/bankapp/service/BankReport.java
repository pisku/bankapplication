package com.luxoft.skurski.bankapp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;

public class BankReport {

	public static void getNumberOfClients(Bank bank) {
		System.out.println("Total number of clients: "
				+ bank.getClients().size());
	}

	public static void getAccountsNumber(Bank bank) {
		int accountsNum = 0;
		for (Client client: bank.getClients()) {
			accountsNum += client.getAccounts().size();
		}
		System.out.println("Total number of accounts: " + accountsNum);
	}

	public static void getClientsSorted(Bank bank) {
		List<Client> clientsList = new ArrayList<Client>(bank.getClients());
		Collections.sort(clientsList);
		System.out.println("SORTED CLIENTS LIST --------------------------------------");
		for (int i=0; i<clientsList.size(); i++) {
			System.out.println(i + ") " + clientsList.get(i).printReport());
		}
	}

	public static void getBankCreditSum(Bank bank) {
		float creditSum = 0.0f;
		for (Client client: bank.getClients()) {
			for (Account account: client.getAccounts()) {
				if (account.getBalance() < 0)
					creditSum += account.getBalance();
			}
		}
		System.out.println("Total credit sum: " + creditSum);
	}

	public static void getClientsByCity(Bank bank) {
		Map<String, Set<Client>> clientsByCityMap = new TreeMap<String, Set<Client>>();
		
		System.out.println("CLIENTS BY CITY --------------------------------------");
		for (Client client: bank.getClients()) {
			Set<Client> clientsSet = clientsByCityMap.get(client.getCity());
			if (clientsSet == null) {
				clientsSet = new TreeSet<Client>();
				clientsByCityMap.put(client.getCity(), clientsSet);
			}
			clientsSet.add(client);
		}
		
		for (Map.Entry<String, Set<Client>> entry: clientsByCityMap.entrySet()) {
			System.out.println(entry.getKey() + ": ");
			for (Client client: entry.getValue()) {
				System.out.println(client.printReport());
			}
		}
	}

}
