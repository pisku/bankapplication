package com.luxoft.skurski.bankapp.util;

public class Validation {

	public static void checkForNull(Object obj) {
		if (obj == null) {
			throw new NullPointerException();
		}
	}
	
	public static boolean checkIsEmail(String email) {
		return email.matches(
			"^[A-Za-z\\.-0-9]{2,}@[A-Za-z\\.-0-9]{2,}\\.[A-Za-z]{2,3}$");
	}
	
	public static boolean checkIsFullName(String name) {
		return name.matches("([A-Z]\\w+ [A-Z]\\w+)");
	}

	public static boolean checkAreWords(String name) {
		return name.matches("^[a-zA-Z ]*$");
	}

	public static boolean checkIsPhone(String phone) {
		return phone.matches("^[0-9\\-]{7,11}$");
	}
	
	public static boolean checkIsWholeNumber(String number) {
		return number.matches("\\d+");
	}
	
	public static boolean checkIsPositiveNumber(String number) {
		return number.matches("\\d+(\\.\\d+)?");
	}
	
	public static boolean checkIsExpectedNumber(String number, int lastIndex) {
		if (!checkIsWholeNumber(number))
				return false;
		
		for (int i=0; i<=lastIndex; i++) {
			if (Integer.parseInt(number) == i)
				return true;
		}
		return false;
	}
	
	public static boolean checkIsNumberFromArray(String number, int[] numbers) {
		if (!checkIsWholeNumber(number))
			return false;
		
		for (int i=0; i<numbers.length; i++) {
			if (Integer.parseInt(number) == numbers[i])
				return true;
		}
		return false;
	}
}