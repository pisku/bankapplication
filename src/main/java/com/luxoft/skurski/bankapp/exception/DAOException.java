package com.luxoft.skurski.bankapp.exception;

import java.sql.SQLException;

public class DAOException extends SQLException {
	private static final long serialVersionUID = -1793212488402239362L;
	private Exception exception = null;
	private String message = null;

	public DAOException(String message) {
		this.message = message;
	}

	public DAOException(Exception e) {
		this.exception = e;
	}
	
	public String printMessage() {
		StringBuilder sb = new StringBuilder();
		if (message != null) {
			sb.append(message);
			sb.append("\n");
		}
		if (exception != null) {
			sb.append(exception.getMessage());
		}
		return sb.toString();
	}
}
