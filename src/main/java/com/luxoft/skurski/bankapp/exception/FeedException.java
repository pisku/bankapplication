package com.luxoft.skurski.bankapp.exception;

public class FeedException extends RuntimeException {

	private static final long serialVersionUID = 5857984859930773172L;

	public FeedException(String message) {
         super(message);
    }
}
