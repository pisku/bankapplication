package com.luxoft.skurski.bankapp.exception;

public class BankNotFoundException extends BankException {
	private static final long serialVersionUID = -1237564917360811041L;
	
	private String name = null;
	
	public BankNotFoundException() {}
	
	public BankNotFoundException(String name) {
		this.name = name;
	}
	
	public String printMessage() {
		if (name != null) 
			return "Bank with that name not exist: " + name;
		else 
			return "No bank";
	}
}