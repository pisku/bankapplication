package com.luxoft.skurski.bankapp.exception;

public class ClientNotExistsException extends BankException {

	private static final long serialVersionUID = 7471041238018929740L;
	private String name;
	
	public ClientNotExistsException() {}
	
	public ClientNotExistsException(String name) {
		this.name = name;
	}
	
	public String printMessage() {
		if (name != null) 
			return "Client with that name not exist: " + name;
		else 
			return "No client";
	}
}
