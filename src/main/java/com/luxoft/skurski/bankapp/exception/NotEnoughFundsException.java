package com.luxoft.skurski.bankapp.exception;

import com.luxoft.skurski.bankapp.model.Account;

public class NotEnoughFundsException extends BankException {

	private static final long serialVersionUID = 8634665546378752616L;
	private float balance;
    private float amount;
	private Account clientAccount;
	private float withdraw;

	public NotEnoughFundsException(Account clientAccount, float withdraw) {
		this.clientAccount = clientAccount;
		this.balance = clientAccount.getBalance();
		this.amount = clientAccount.maximumAmountToWithdraw();
		this.withdraw = withdraw;
	}

	public float getBalance() {
		return balance;
	}

	public float getAmount() {
		return amount;
	}

	public float getWithdraw() {
		return withdraw;
	}

	public Account getClientAccount() {
		return clientAccount;
	}
	
	public String printMessage() {
		return "Balance: " + balance + ", max amount to withdraw: " 
				+ amount + ", you want to withdraw: " + withdraw;
	}
}
