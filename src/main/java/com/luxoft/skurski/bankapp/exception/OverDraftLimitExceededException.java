package com.luxoft.skurski.bankapp.exception;

import com.luxoft.skurski.bankapp.model.Account;

public class OverDraftLimitExceededException extends NotEnoughFundsException {

	private static final long serialVersionUID = -8788487512704578141L;

	public OverDraftLimitExceededException(Account clientAccount, float withdraw) {
		super(clientAccount, withdraw);
	}

}
