package com.luxoft.skurski.bankapp.exception;

public class ClientExistsException extends BankException {

	private static final long serialVersionUID = 6687787146826534711L;
	private String name;
	
	public ClientExistsException(String name) {
		this.name = name;
	}
	
	public String printMessage() {
		return "Client with that name already exist: " + name;
	}
}
