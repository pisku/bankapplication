package com.luxoft.skurski.bankapp.dao;

import java.util.List;

import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Account;

public interface AccountDAO {
	
	public void save(Account account) throws DAOException;

	public void save(List<Account> accounts) throws DAOException;

	public void add(Account account) throws DAOException;

    public void add(List<Account> accounts) throws DAOException;

	public void removeByClientId(int idClient) throws DAOException;

	public List<Account> getClientAccounts(int idClient) throws DAOException;
}