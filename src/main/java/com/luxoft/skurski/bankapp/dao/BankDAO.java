package com.luxoft.skurski.bankapp.dao;

import com.luxoft.skurski.bankapp.exception.BankNotFoundException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.BankInfo;

public interface BankDAO {
	/**
	 * Finds Bank by its name
     * Do not load the list of the clients
	 */
	Bank getBankByName(String name) throws DAOException, BankNotFoundException;

    /**
     * Fill only bank table, do not save the list of clients
     */
	void save(Bank bank) throws DAOException, BankNotFoundException;

	void remove(Bank bank) throws DAOException;

	BankInfo getBankInfo(Bank bank);

    /**
     * Save all bank data with the list of all clients
     */
    void saveAll(Bank bank) throws DAOException, BankNotFoundException;

    /**
     * Finds Bank by its name
     * Load all data (list of clients etc.)
     */
    Bank loadBank(String name) throws DAOException, BankNotFoundException;
}
