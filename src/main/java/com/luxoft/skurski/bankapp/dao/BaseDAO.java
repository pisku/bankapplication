package com.luxoft.skurski.bankapp.dao;

import java.sql.Connection;

import com.luxoft.skurski.bankapp.exception.DAOException;

public interface BaseDAO {
    Connection openConnection() throws DAOException;
    void closeConnection();
    void setTestEnv(boolean test);
    int insert(String table, String[] columns, String[] values) throws DAOException;
}