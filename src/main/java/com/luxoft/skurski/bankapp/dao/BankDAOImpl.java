package com.luxoft.skurski.bankapp.dao;

import java.sql.*;
import java.util.List;
import java.util.Set;

import com.luxoft.skurski.bankapp.exception.BankNotFoundException;
import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.BankInfo;
import com.luxoft.skurski.bankapp.model.Client;

public class BankDAOImpl extends BaseDAOImpl implements BankDAO {
    private ClientDAO clientDAO = new ClientDAOImpl();

    @Override
    public void setTestEnv(boolean test) {
        super.setTestEnv(test);
        ((BaseDAO) clientDAO).setTestEnv(true);
    }

	@Override
	public Bank getBankByName(String name) throws DAOException, BankNotFoundException {
		Bank bank = null;
		String sql = "SELECT id,name FROM bank WHERE name=?";
		PreparedStatement stmt;
		try {
			stmt = openConnection().prepareStatement(sql);
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				int id = result.getInt("id");
				bank = new Bank(name);
				bank.setId(id);
			} else {
				throw new BankNotFoundException(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e);
		} finally {
			closeConnection();
		}
		return bank;
	}

	@Override
	public void save(Bank bank) throws DAOException, BankNotFoundException {

		String sql = "INSERT INTO bank (name) VALUES (?)";
		PreparedStatement stmt = null;

		try {
			stmt = openConnection().prepareStatement(sql);
			stmt.setString(1, bank.getName());
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e);
		} finally {
			closeConnection();
		}
	}

	@Override
	public void remove(Bank bank) throws DAOException {
		String sql = "DELETE FROM bank WHERE id=?";
		PreparedStatement stmt = null;
		
		try {
			stmt = openConnection().prepareStatement(sql);
			stmt.setInt(1, bank.getId());
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e);
		} finally {
			closeConnection();
		}
	}

	@Override
	public BankInfo getBankInfo(Bank bank) {
		return new BankInfo(bank);
	}

    @Override
    public void saveAll(Bank bank) throws DAOException, BankNotFoundException {
        if (isObjectInDbByName(bank.getName(),"bank")) {
            updateBank(bank);
            return;
        }

        String sql = "INSERT INTO bank (name) VALUES (?)";
        PreparedStatement stmt = null;

        try {
            stmt = openConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, bank.getName());
            int res = stmt.executeUpdate();

            if (res == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                if (!keys.next()) {
                    throw new DAOException("Can not save the list of clients, " +
                            "bank name: " + bank.getName());
                }

                int bankId = keys.getInt(1);
                bank.setId(bankId);
                Set<Client> clients = bank.getClients();
                for (Client client: clients) {
                    client.setBankId(bankId);
                    clientDAO.save(client);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
	public Bank loadBank(String name) throws DAOException, BankNotFoundException {
		Bank bank = null;
		String sql = "SELECT id,name FROM bank WHERE name=?";
		PreparedStatement stmt;

        try {
			stmt = openConnection().prepareStatement(sql);
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				int id = result.getInt("id");
				bank = new Bank(name);
				bank.setId(id);

                ResultSet test = openConnection().createStatement().executeQuery(
                        "select * from bank"
                );
                ResultSetMetaData meta = test.getMetaData();
                while (test.next()) {
                    for (int j=1; j<=meta.getColumnCount(); j++) {
                        System.out.println("test: " + test.getObject(j));
                    }
                }

				List<Client> clients = clientDAO.getAllClients(bank);
				for (Client client: clients) {
					try {
						bank.addClient(client);
					} catch (ClientExistsException e) {
						System.out.println(e.printMessage());
					}
				}
			} else {
				throw new BankNotFoundException(name);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DAOException(e);
		} finally {
			closeConnection();
		}
		return bank;
	}

    private void updateBank(Bank bank) throws DAOException {
        PreparedStatement stmt = null;

        try {
            String sql = "UPDATE bank SET name = ?" +
                    "WHERE id = ?";
            stmt = openConnection().prepareStatement(sql);
            stmt.setString(1, bank.getName());
            stmt.setInt(2, bank.getId());
            stmt.executeUpdate();

            System.out.println(bank.getId());
            System.out.println("update");
            for (Client client: bank.getClients()) {
                client.setBankId(bank.getId());
                clientDAO.save(client);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }
    }
}
