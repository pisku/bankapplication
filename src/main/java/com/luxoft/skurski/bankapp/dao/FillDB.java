package com.luxoft.skurski.bankapp.dao;

import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;
import java.sql.Statement;

public class FillDB extends BaseDAOImpl {
    private BaseDAO db = new BaseDAOImpl();
    private final String SQL_SCRIPT_FILE = "src/main/resources/db/db_changes.sql";
    private String db_file = null;

    public FillDB() {
        db_file = SQL_SCRIPT_FILE;
    }

    public FillDB(String path) {
        this.db_file = path;
    }

    public void setTestEnv(boolean test) {
        if (test) {
            db.setTestEnv(test);
        }
    }

    public void init() {
        try {
            RunScript.execute(db.openConnection(), new FileReader(db_file));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            db.closeConnection();
        }
    }

    public void clean() {
        Statement stmt = null;

        try {
            stmt = db.openConnection().createStatement();
            stmt.executeUpdate("DROP TABLE account;");
            stmt.executeUpdate("DROP TABLE client;");
            stmt.executeUpdate("DROP TABLE bank;");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            db.closeConnection();
        }
    }
}
