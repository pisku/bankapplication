package com.luxoft.skurski.bankapp.dao;

import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.CheckingAccount;
import com.luxoft.skurski.bankapp.model.SavingAccount;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountDAOImpl extends BaseDAOImpl implements AccountDAO {

    @Override
    public void save(Account account) throws DAOException {
        PreparedStatement stmt = null;

        try {
            stmt = openConnection().prepareStatement(
                    "UPDATE account SET " +
                    "balance = ?," +
                    "overdraft = ?," +
                    "type = ?," +
                    "client_id = ? " +
                    "WHERE id = ?;"
            );
            stmt.setFloat(1, account.getBalance());
            stmt.setFloat(2, account.getOverdraft());
            stmt.setString(3, account.getType());
            stmt.setInt(4, account.getClientId());
            stmt.setInt(5, account.getId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
    public void save(List<Account> accounts) throws DAOException {
        PreparedStatement stmt = null;

        try {
            stmt = openConnection().prepareStatement(
                    "UPDATE account SET " +
                            "balance = ?," +
                            "overdraft = ?," +
                            "type = ?," +
                            "client_id = ? " +
                            "WHERE id = ?;"
            );

            for (Account account: accounts) {
                stmt.setFloat(1, account.getBalance());
                stmt.setFloat(2, account.getOverdraft());
                stmt.setString(3, account.getType());
                stmt.setInt(4, account.getClientId());
                stmt.setInt(5, account.getId());
                stmt.addBatch();
            }

            final int[] results = stmt.executeBatch();
            for (int i = 0; i < results.length; i++) {
                if (results[i] == Statement.EXECUTE_FAILED) {
                    System.out.println("Batch #" + i + " - Failed.");
                } else if (results[i] == Statement.SUCCESS_NO_INFO) {
                    System.out.println("Batch #" + i + " - Succeded with no results.");
                } else {
                    System.out.println("Batch #" + i + " - Affected " + results[i] + " rows.");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }
    }

    @Override
    public void add(Account account) throws DAOException {
        PreparedStatement stmt = null;

        try {
            stmt = openConnection().prepareStatement(
                    "INSERT INTO account (balance,overdraft,type,client_id) " +
                            "VALUES (?,?,?,?)"
            );
            stmt.setFloat(1, account.getBalance());
            stmt.setFloat(2, account.getOverdraft());
            stmt.setString(3, account.getType());
            stmt.setInt(4, account.getClientId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
    public void add(List<Account> accounts) throws DAOException {
        PreparedStatement stmt = null;

        try {
            stmt = openConnection().prepareStatement(
                    "INSERT INTO account (balance,overdraft,type,client_id) " +
                            "VALUES (?,?,?,?)"
            );

            for (Account account: accounts) {
                stmt.setFloat(1, account.getBalance());
                stmt.setFloat(2, account.getOverdraft());
                stmt.setString(3, account.getType());
                stmt.setInt(4, account.getClientId());
                stmt.addBatch();
            }

            final int[] results = stmt.executeBatch();
            for (int i = 0; i < results.length; i++) {
                if (results[i] == Statement.EXECUTE_FAILED) {
                    System.out.println("Batch #" + i + " - Failed.");
                } else if (results[i] == Statement.SUCCESS_NO_INFO) {
                    System.out.println("Batch #" + i + " - Succeded with no results.");
                } else {
                    System.out.println("Batch #" + i + " - Affected " + results[i] + " rows.");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }
    }

    @Override
    public void removeByClientId(int idClient) throws DAOException {
        PreparedStatement stmt = null;

        try {
            stmt = openConnection().prepareStatement(
                    "DELETE FROM account " +
                    "WHERE client_id = ?"
            );
            stmt.setFloat(1, idClient);
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
    public List<Account> getClientAccounts(int idClient) throws DAOException {
        PreparedStatement stmt = null;
        ResultSet result = null;
        List<Account> accounts = new ArrayList<Account>();

        try {
            stmt = openConnection().prepareStatement(
                    "SELECT id,balance,overdraft,type " +
                    "FROM account " +
                    "WHERE client_id = ?"
            );
            stmt.setInt(1, idClient);
            result = stmt.executeQuery();
            while (result.next()) {
                accounts.add(createAccount(
                        idClient,
                        result.getInt(1),
                        result.getFloat(2),
                        result.getFloat(3),
                        result.getString(4)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                result.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }

        return accounts;
    }

    private Account createAccount(int clientId, int accountId, float balance,
                                  float overdraft, String type) {
        Account account = null;
        switch (type) {
            case "c":
                account = new CheckingAccount(accountId,clientId,
                                balance,overdraft);
                break;
            case "s":
                account = new SavingAccount(accountId,clientId,balance);
                break;
        }

        return account;
    }
}
