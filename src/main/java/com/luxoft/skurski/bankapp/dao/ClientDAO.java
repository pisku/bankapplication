package com.luxoft.skurski.bankapp.dao;

import java.util.List;
import java.util.Set;

import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;

public interface ClientDAO {
	/**
	 * Return client by its name, initialize client accounts.
	 * 
	 * @param bank
	 * @param name
	 * @return
	 */
	Client findClientByName(Bank bank, String name) throws DAOException, ClientNotExistsException;

	/**
	 * Returns the list of all clients of the Bank And their accounts
	 * 
	 * @param bank
	 * @return
	 */
	List<Client> getAllClients(Bank bank) throws DAOException;

	/**
	 * Method should insert new Client (if id == null) Or update client in
	 * database
	 * 
	 * @param client
	 */
	void save(Client client) throws DAOException;

	/**
	 * Method removes client from Database
	 * 
	 * @param client
	 */
	void remove(Client client) throws DAOException;
}
