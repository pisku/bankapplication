package com.luxoft.skurski.bankapp.dao;

import java.sql.*;

import com.luxoft.skurski.bankapp.exception.DAOException;

public class BaseDAOImpl implements BaseDAO {
	private final String DRIVER = "org.h2.Driver";
	private final String DB_URL = "jdbc:h2:~/";
	private final String PRODUCTION_DB_NAME = "bankapp";
	private final String TEST_DB_NAME = "bankapp_test";
	private final String USER = "sa";
	private final String PASS = "";
    private String dbName;
	private Connection conn = null;

    public BaseDAOImpl() {
        dbName = PRODUCTION_DB_NAME;
    }

    public void setTestEnv(boolean test) {
        if (test) {
            dbName = TEST_DB_NAME;
        }
    }

    public String getDBName() {
        return dbName;
    }

	public Connection openConnection() throws DAOException {
		try {
			Class.forName(DRIVER);
            conn = DriverManager.getConnection(DB_URL+dbName, USER, PASS);
			return conn;
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			throw new DAOException(e);
		}
	}

	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int insert(String table, String[] columns, String[] values) throws DAOException {
        int id = 0;
		PreparedStatement stmt = null;
        ResultSet key = null;
		StringBuilder sqlBuilder = new StringBuilder("INSERT INTO ");
		sqlBuilder.append(table + "(");
		char coma = ' ';
		for (String col: columns) {
			sqlBuilder.append(coma);
			sqlBuilder.append(col);
            coma = ',';
		}
        sqlBuilder.append(") VALUES (");
        coma = ' ';
        for (String val: values) {
            sqlBuilder.append(coma);
            sqlBuilder.append("?");
            coma = ',';
        }
        sqlBuilder.append(");");

		try {
			stmt = openConnection().prepareStatement(sqlBuilder.toString(), Statement.RETURN_GENERATED_KEYS);
            for (int i=0; i<values.length; i++) {
                stmt.setString(i+1, values[i]);
            }

            id = stmt.executeUpdate();
            if (id == 0) {
                throw new SQLException("No data inserted.");
            }

            key = stmt.getGeneratedKeys();
            if (key.next()) {
                id = key.getInt(1);
            }
		} catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        }

        return id;
    }

    public boolean isObjectInDbByName(String whereName, String tableName) {
        String sql = "SELECT id FROM " + tableName + " WHERE name=?";
        PreparedStatement stmt = null;
        ResultSet result = null;
        try {
            stmt = openConnection().prepareStatement(sql);
            stmt.setString(1, whereName);
            result = stmt.executeQuery();
            if (result.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }

        return false;
    }

    public static void main(String[] args) {
        BaseDAOImpl dao = new BaseDAOImpl();
        dao.setTestEnv(true);
        FillDB fill = new FillDB();
        fill.setTestEnv(true);
        fill.init();
        try {
            dao.insert("account",
                    new String[]{"balance", "overdraft"}, new String[]{"4", "45"});
        } catch (DAOException e) {
            e.printStackTrace();
        }

        fill.clean();
    }
}