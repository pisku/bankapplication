package com.luxoft.skurski.bankapp.dao;

import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;
import com.luxoft.skurski.bankapp.exception.DAOException;
import com.luxoft.skurski.bankapp.model.Account;
import com.luxoft.skurski.bankapp.model.Bank;
import com.luxoft.skurski.bankapp.model.Client;
import com.luxoft.skurski.bankapp.model.Gender;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClientDAOImpl extends BaseDAOImpl implements ClientDAO {
    private AccountDAO accountDAO = new AccountDAOImpl();

    @Override
    public void setTestEnv(boolean test) {
        super.setTestEnv(test);
        ((BaseDAO) accountDAO).setTestEnv(true);
    }

    @Override
    public Client findClientByName(Bank bank, String name) throws DAOException, ClientNotExistsException {
        PreparedStatement stmt = null;
        ResultSet result = null;
        Client client = null;

        try {
            stmt = openConnection().prepareStatement(
                    "SELECT id,bank_id,name,email," +
                    "phone,city,gender,initial_overdraft " +
                    "FROM client " +
                    "WHERE bank_id=? " +
                    "AND name=?;"
            );
            stmt.setInt(1, bank.getId());
            stmt.setString(2, name);
            result = stmt.executeQuery();
            if (result.next()) {
                client = new Client(result.getInt("id"), result.getInt("bank_id"),
                        result.getString("name"), Gender.parseGender(result.getString("gender")),
                        result.getFloat("initial_overdraft"), result.getString("email"),
                        result.getString("phone"), result.getString("city"));
                client.setAccounts(accountDAO.getClientAccounts(client.getId()));
                client.setDefaultActiveAccount();

            } else {
                throw new ClientNotExistsException(name);
            }

            client.setAccounts(accountDAO.getClientAccounts(client.getId()));

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }

        return client;
    }

    @Override
    public List<Client> getAllClients(Bank bank) throws DAOException {
        PreparedStatement stmt = null;
        ResultSet result = null;
        List<Client> clients = new ArrayList<Client>();

        try {
            stmt = openConnection().prepareStatement(
                    "SELECT id,bank_id,name,email," +
                            "phone,city,gender,initial_overdraft " +
                            "FROM client " +
                    "WHERE bank_id = ?"
            );
            stmt.setInt(1, bank.getId());
            result = stmt.executeQuery();
            while (result.next()) {
                Client client = new Client(result.getInt("id"), result.getInt("bank_id"),
                        result.getString("name"), Gender.parseGender(result.getString("gender")),
                        result.getFloat("initial_overdraft"), result.getString("email"),
                        result.getString("phone"), result.getString("city"));
                client.setAccounts(accountDAO.getClientAccounts(client.getId()));
                client.setDefaultActiveAccount();
                clients.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                result.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }

        return clients;
    }

    @Override
    public void save(Client client) throws DAOException {
        PreparedStatement stmt = null;

        try {

            if (!isObjectInDbByName(client.getName(),"client")) {
                add(client);
                return;
            }

            String sql = "UPDATE client SET " +
                            "bank_id = ?," +
                            "name = ?," +
                            "email = ?," +
                            "phone = ?," +
                            "city = ?," +
                            "gender = ?," +
                            "initial_overdraft = ? " +
                        "WHERE id = ?;";

            stmt = openConnection().prepareStatement(sql);
            stmt.setInt(1, client.getBankId());
            stmt.setString(2, client.getName());
            stmt.setString(3, client.getEmail());
            stmt.setString(4, client.getPhone());
            stmt.setString(5, client.getCity());
            stmt.setString(6, client.getGender().getSign());
            stmt.setFloat(7, client.getInitialOverdraft());
            stmt.setInt(8, client.getId());
            stmt.executeUpdate();

            accountDAO.save(new ArrayList<Account>(client.getAccounts()));

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }
    }

    public void add(Client client) throws DAOException {
        PreparedStatement stmt = null;

        try {
            String sql = "INSERT INTO client " +
                    "(bank_id, name, email, phone, city, gender, initial_overdraft) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?);";

            stmt = openConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, client.getBankId());
            stmt.setString(2, client.getName());
            stmt.setString(3, client.getEmail());
            stmt.setString(4, client.getPhone());
            stmt.setString(5, client.getCity());
            stmt.setString(6, client.getGender().getSign());
            stmt.setFloat(7, client.getInitialOverdraft());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                if (!keys.next()) {
                    throw new DAOException("Can not save the list of accounts, " +
                            "client name: " + client.getName());
                }

                int clientId = keys.getInt(1);
                for (Account account: client.getAccounts()) {
                    account.setClientId(clientId);
                }
                accountDAO.add(new ArrayList<Account>(client.getAccounts()));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }
    }

    @Override
    public void remove(Client client) throws DAOException {
        PreparedStatement stmt = null;

        try {
            accountDAO.removeByClientId(client.getId());

            stmt = openConnection().prepareStatement(
                    "DELETE FROM client WHERE id = ?;"
            );
            stmt.setInt(1, client.getId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException(e);
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeConnection();
        }
    }

}
