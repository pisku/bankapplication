package com.luxoft.skurski.bankapp.model;

import com.luxoft.skurski.bankapp.model.Client;

import java.util.*;

public class BankInfo {
    /**
     * Total number of clients of the bank
     */
    private int numberOfClients;

    /**
     * The sum of all accounts of all clients
     */
    private double totalAccountSum;

    /**
     * List of clients by the city
     */
    private Map<String, List<Client>> clientsByCity;

    private Bank bank;

    public BankInfo(Bank bank) {
        this.bank = bank;
        if (!bank.getClients().isEmpty()) {
            numberOfClients = bank.getClients().size();
            totalAccountSum = calculateTotalAccountSum(bank.getClients());
            clientsByCity = createClientsByCity();
        }
    }

    public String printInfo() {
        StringBuilder sb = new StringBuilder();

        sb.append("Total number of clients: ");
        sb.append(numberOfClients).append("\n");
        sb.append("Total account balance sum: ");
        sb.append(totalAccountSum).append("\n");
        sb.append("------- Clients by city --------").append("\n");

        for (Map.Entry<String, List<Client>> entry: clientsByCity.entrySet()) {
            sb.append(entry.getKey() + ": ").append("\n");
            for (Client client: entry.getValue()) {
                sb.append(client.printReport()).append("\n");
            }
        }

        return sb.toString();
    }

    private double calculateTotalAccountSum(Set<Client> clientsSet) {
        double totalSum = 0f;
        for (Client client: clientsSet) {
            for (Account account: client.getAccounts()) {
                totalSum += account.getBalance();
            }
        }
        return totalSum;
    }

    private Map<String, List<Client>> createClientsByCity() {
        Map<String, List<Client>> clientsByCityMap = new TreeMap<String, List<Client>>();

        for (Client client: bank.getClients()) {
            List<Client> clientsList = clientsByCityMap.get(client.getCity());
            if (clientsList == null) {
                clientsList = new ArrayList<Client>();
                clientsByCityMap.put(client.getCity(), clientsList);
            }
            clientsList.add(client);
        }

        return clientsByCityMap;
    }


}