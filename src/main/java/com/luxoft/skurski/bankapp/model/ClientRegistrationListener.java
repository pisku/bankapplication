package com.luxoft.skurski.bankapp.model;

public interface ClientRegistrationListener {
	public void onClientAdded(Client client);
}
