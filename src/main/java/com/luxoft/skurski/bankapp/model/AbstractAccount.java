package com.luxoft.skurski.bankapp.model;

import java.io.Serializable;
import java.util.Map;

import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;

abstract class AbstractAccount implements Account, Serializable {
	private static final long serialVersionUID = -4677601489269999334L;
	private float balance;
	private int id;
    private String type;
    private int clientId;
	private static int counter = 0;
	
	AbstractAccount(String type) {
        this.type = type;
		balance = 0;
		id = counter++;
	}

	AbstractAccount(float balance, String type) {
		this.id = counter++;
		this.balance = balance;
        this.type = type;
	}

    /**
     * Use when retrieved data from database
     * @param id
     * @param clientId
     * @param balance
     */
	AbstractAccount(int id, int clientId, float balance, String type) {
		this.id = id;
        this.clientId = clientId;
		this.balance = balance;
        this.type = type;
	}

	@Override
	public void setClientId(int id) {
		this.clientId = id;
	}

    @Override
    public int getClientId() {
        return clientId;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
	public float getBalance() {
		return balance;
	}

	@Override
	public void deposit(float deposit) {
		if (deposit < 0) 
			throw new IllegalArgumentException();
		
		balance += deposit;
	}

	@Override
	public synchronized void withdraw(float withdraw) throws NotEnoughFundsException {
		if (withdraw < 0) {
			throw new IllegalArgumentException();
		}
		if (withdraw > balance) {
			throw new NotEnoughFundsException(this, withdraw);
		}
		
		balance -= withdraw;
	}

	public void setBalance(float balance) throws IllegalArgumentException {
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}

	@Override
	public float decimalValue() {
		return ((float) (Math.round(balance * 100)) / 100);
	}

	@Override
	public void parseFeed(Map<String, String> feed) {
		setBalance(Float.parseFloat(feed.get("balance")));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AbstractAccount that = (AbstractAccount) o;

		return id == that.id;

	}

	@Override
	public int hashCode() {
		return id;
	}
}

