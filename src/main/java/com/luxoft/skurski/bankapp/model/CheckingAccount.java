package com.luxoft.skurski.bankapp.model;

import java.util.Map;

import com.luxoft.skurski.bankapp.exception.OverDraftLimitExceededException;

public class CheckingAccount extends AbstractAccount {

	private static final long serialVersionUID = -2696395068528085812L;
	private float overdraft;
	
	public CheckingAccount() {
		super("c");
	}
	
	public CheckingAccount(float balance) {
		super(balance, "c");
	}
	
	public CheckingAccount(float balance, float overdraft) {
		this(balance);
		this.overdraft = overdraft;
	}

    /**
     * Use when retrieved data from database
     * @param id
     * @param clientId
     * @param balance
     * @param overdraft
     */
	public CheckingAccount(int id, int clientId,
                           float balance, float overdraft) {
		super(id, clientId, balance, "c");
		this.overdraft = overdraft;
	}
	
	public float getOverdraft() {
		return overdraft;
	}

	public void setOverdraft(float overdraft) {
		this.overdraft = overdraft;
	}

	@Override
	public synchronized void withdraw(float withdraw) throws OverDraftLimitExceededException {
		if (withdraw < 0) {
			throw new IllegalArgumentException();
		}
		if (withdraw > getBalance() + overdraft) {
			throw new OverDraftLimitExceededException(this, withdraw);
		}
		setBalance(getBalance() - withdraw);
	}

	@Override
	public String printReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("Account type: Checking Account, ID: ")
			.append(getId())
			.append(", balance: ")
			.append(decimalValue())
			.append(", overdraft: ")
			.append(overdraft);
		return sb.toString();
	}
	
	@Override
	public float maximumAmountToWithdraw(){
		return getBalance() + overdraft;
	}

	@Override
	public String toString() {
		return "CheckingAccount [overdraft=" + overdraft + ", maximumAmountToWithdraw()=" + maximumAmountToWithdraw()
				+ ", getBalance()=" + getBalance() + ", getId()=" + getId() + "]";
	}

	@Override
	public void parseFeed(Map<String, String> feed) {
		super.parseFeed(feed);
		overdraft = Float.parseFloat(feed.get("overdraft"));
	}
	
}

