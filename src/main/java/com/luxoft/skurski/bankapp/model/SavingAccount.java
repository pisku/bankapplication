package com.luxoft.skurski.bankapp.model;

public class SavingAccount extends AbstractAccount {

	private static final long serialVersionUID = -9131067946890048314L;

	public SavingAccount() {
		super("s");
	}
	
	public SavingAccount(float balance) {
		super(balance, "s");
	}

    /**
     * Use when retrieved data from database
     * @param id
     * @param clientId
     * @param balance
     */
	public SavingAccount(int id, int clientId, float balance) {
		super(id, clientId, balance, "s");
	}

	@Override
	public void setBalance(float balance) throws IllegalArgumentException {
		if (balance < 0) {
			throw new IllegalArgumentException();
		}
		super.setBalance(balance);
	}
	
	@Override
	public String printReport() {
		StringBuilder sb = new StringBuilder();
		sb.append("Account type: Saving Account, ID: ")
			.append(getId())
			.append(", balance: ")
			.append(decimalValue());
		return sb.toString();
	}

    @Override
    public float getOverdraft() {
        return 0;
    }

    @Override
	public float maximumAmountToWithdraw(){
	    return getBalance();
	}

	@Override
	public String toString() {
		return "SavingAccount [maximumAmountToWithdraw()=" + maximumAmountToWithdraw() + ", getBalance()="
				+ getBalance() + ", getId()=" + getId() + "]";
	}
}

