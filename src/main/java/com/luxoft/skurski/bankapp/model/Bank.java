package com.luxoft.skurski.bankapp.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.luxoft.skurski.bankapp.dao.NoDB;
import com.luxoft.skurski.bankapp.exception.ClientExistsException;
import com.luxoft.skurski.bankapp.exception.ClientNotExistsException;

public class Bank implements Report {
	@NoDB
	private int id;

	private String name = "PS Bank"; //"Default";

	private Set<Client> clients = new HashSet<Client>();

	@NoDB
	private Map<String, Client> clientsMap = new HashMap<String, Client>();

	@NoDB
	private List<ClientRegistrationListener> listeners = new ArrayList<ClientRegistrationListener>();
	
	public Bank() {
		listeners.add(new PrintClientListener());
		listeners.add(new EmailNotificationListener());
	}
	
	public Bank(String bankName) {
		this();
		this.name = bankName;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Client> getClients() {
		return Collections.unmodifiableSet(clients);
	}

	public void addClient(Client client) throws ClientExistsException {
		if (clients.contains(client)) {
			throw new ClientExistsException(client.getName());
		}
		clients.add(client);
		clientsMap.put(client.getName(), client);
		notifyListeners(client);
	}
	
	public void removeClient(Client client) {
		if (!clients.isEmpty()) {
			clients.remove(client);
		}
	}
	
    public void parseFeed(Map<String, String> feed) {
        String name = feed.get("name"); 
        
        Client client = clientsMap.get(name);
        if (client == null) {
              client = new Client(name, Float.parseFloat(feed.get("overdraft")));
              clients.add(client);
              clientsMap.put(name, client);
        }
        
        client.parseFeed(feed);
   }
	
	@Override
	public String printReport() {
		StringBuilder sb = new StringBuilder();
		for (Client client : clients) {
			sb.append(client.printReport());
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public Client findClient(String name) throws ClientNotExistsException {
		Client searchedClient = null;
		for (Client client: clients) {
			if (client.getName().equals(name))
				searchedClient = client;
		}
		if (searchedClient == null) 
			throw new ClientNotExistsException(name);
		
		return searchedClient;
	}
	
	private class PrintClientListener implements ClientRegistrationListener {
		@Override
		public void onClientAdded(Client client) {
			System.out.println("Client: " + client.getClientSalutation());
		}
	}
	
	private class EmailNotificationListener implements ClientRegistrationListener {
		@Override
		public void onClientAdded(Client client) {
			System.out.println("Notification email for client: " + client.getClientSalutation() + " to be sent");
		}
	}
	
	private void notifyListeners(Client client) {
		for (ClientRegistrationListener listener: listeners) {
			listener.onClientAdded(client);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bank other = (Bank) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

}

