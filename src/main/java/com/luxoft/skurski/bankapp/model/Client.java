package com.luxoft.skurski.bankapp.model;

import java.io.Serializable;
import java.util.*;

import com.luxoft.skurski.bankapp.exception.FeedException;

public class Client implements Report, Comparable<Client>, Serializable {
	private static final long serialVersionUID = -314495632608649981L;
	private int id;
	private String name;
	private String email;
	private String phone;
	private String city;
	private Gender gender;
	private Set<Account> accounts = new HashSet<Account>();
	private Account activeAccount;
	private float initialOverdraft;
	private int bankId;
	
	public Client(String name, float initialOverdraft) {
		this.name = name;
		this.initialOverdraft = initialOverdraft;
	}
	
	public Client(String name, Gender gender, float initialOverdraft) {
		this.name = name;
		this.gender = gender;
		this.initialOverdraft = initialOverdraft;
	}
	
	public Client(String name, Gender gender,float initialOverdraft,
			String email, String phone) {
		this(name, gender, initialOverdraft);
		this.email = email;
		this.phone = phone;
	}

	public Client(String name, Gender gender, float initialOverdraft,
			String email, String phone, String city) {
		this(name, gender, initialOverdraft, email, phone);
		this.city = city;
	}

	/**
	 * Database constructor
     */
	public Client(int id, int bankId, String name, Gender gender, float initialOverdraft,
			            String email, String phone, String city) {
		this(name, gender, initialOverdraft, email, phone, city);
		this.id = id;
        this.bankId = bankId;
	}

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public float getInitialOverdraft() {
        return initialOverdraft;
    }

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setActiveAccount(Account account) {
		this.activeAccount = account;
	}

    public void setDefaultActiveAccount() {
        if (!accounts.isEmpty()) {
            activeAccount = accounts.iterator().next();
        }
    }
	
	public Account getActiveAccount() {
		return activeAccount;
	}
	
	public float getBalance() {
		return activeAccount.getBalance();
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void addAccount(Account account) {
		if (account instanceof CheckingAccount) {
			CheckingAccount checkingAccount = (CheckingAccount) account;
			checkingAccount.setOverdraft(initialOverdraft);
			accounts.add(checkingAccount);
		} else {
			accounts.add(account);
		}
		activeAccount = account;
	}

	public Set<Account> getAccounts() {
		return Collections.unmodifiableSet(accounts);
	}
	
	public Account searchAccount(int id) {
		for (Account account: accounts) {
			if (account.getId() == id) 
				return account;
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Gender getGender() {
		return gender;
	}

	public String getClientSalutation() {
		return gender.printSalutation() + " " + name;
	}
	
    public void parseFeed(Map<String, String> feed) {
        String accountType = feed.get("accounttype");
        Account account = getAccount(accountType);
        gender = Gender.parseGender(feed.get("gender"));

    	account.parseFeed(feed);
    }

    public void setAccounts(Collection<Account> accounts) {
        this.accounts.addAll(accounts);
    }

    /**
     * This method finds account by its type or create a new one
     */
    private Account getAccount(String accountType) throws FeedException {
        for (Account account: accounts) {
        	 if (account.getType().equals(accountType)) {
                 return account;
        	 }
         }
         return createAccount(accountType);
    }
    
    /**
     * This method creates account by its type
     */
    private Account createAccount(String accountType) throws FeedException {
    	Account account = null;
    	if ("s".equals(accountType)) {
    		account = new SavingAccount();
    	} else if ("c".equals(accountType)) {
    		account = new CheckingAccount();
    	} else {
    		throw new FeedException("Account type not found " + accountType);
    	}  
    	accounts.add(account);
    	
    	return account;
    }

	@Override
	public String printReport() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClientSalutation());
		
		for (Account account: accounts) {
			sb.append("\n" + account.printReport());
		}
		return sb.toString();
	}
	
	@Override
	public int compareTo(Client client) {
		if (this == client) 
			return 0;
		return this.name.compareTo(client.name);
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClientSalutation());
		
		for (Account account: accounts) 
			sb.append(account);
		
		return sb.toString();
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}

