package com.luxoft.skurski.bankapp.model;

import java.io.Serializable;

public enum Gender implements Serializable {
	MALE("Mr.", "m"), FEMALE("Ms.", "f");
	
	private String salutation;
	private String sign;

	Gender(String salutation, String sign) {
		this.salutation = salutation;
		this.sign = sign;
	}
	
	String printSalutation() {
		return salutation;
	}
	
	public static Gender parseGender(String sex) {
		if (sex.equals("m"))
			return Gender.MALE;
		else if (sex.equals("f"))
			return Gender.FEMALE;
		
		return null;
	}

	public String getSign() {
		return sign;
	}
}

