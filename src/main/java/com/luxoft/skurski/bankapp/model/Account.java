package com.luxoft.skurski.bankapp.model;

import java.util.Map;

import com.luxoft.skurski.bankapp.exception.NotEnoughFundsException;

public interface Account extends Report {
	public float getBalance();
	public float getOverdraft();
	public void deposit(float deposit);
	public void withdraw(float withdraw) throws NotEnoughFundsException;
	public float maximumAmountToWithdraw();
	public float decimalValue();
	public int getId();
    public void setId(int id);
	public String getType();
    public int getClientId();
	public void setClientId(int clientId);
	public void parseFeed(Map<String,String> feed);
}

