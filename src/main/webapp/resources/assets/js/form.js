$(document).ready(function() {

    $('#login').submit(function(event) {
        var name = $('#clientName').val();
        var password = $('#password').val();

        if (!isFilled(name)) {
            event.preventDefault();
            $('span.name').html(' Please fill the name.');
        }
        if (!isFilled(password)) {
            event.preventDefault();
            $('span.password').html(' Please fill the password.');
        }
    });

    $('#withdraw-form').submit(function(event) {
        var withdraw = $('#withdraw').val();
        if (!isNumeric(withdraw)) {
            event.preventDefault();
            $('span.withdraw').html('Provide valid number!');
        }
    });

    $('#deposit-form').submit(function(event) {
        var deposit = $('#deposit').val();
        if (!isNumeric(deposit)) {
            event.preventDefault();
            $('span.deposit').html('Provide valid number!');
        }
    });

    $('input').on('input', function(e) {
        $('span').html('');
    });
});