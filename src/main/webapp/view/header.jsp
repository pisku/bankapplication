<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="header">
    <a href="/">Bank Application</a>
    <c:if test="${!empty sessionScope.clientName}">
        <a class="menu" href="/logout">Logout</a>
        <a class="menu" href="/atm">ATM</a>
    </c:if>
</div>

<div class="container">
