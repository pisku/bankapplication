<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="head.jsp"/>
<jsp:include page="header.jsp"/>

<div class="client-info">
    <p>Your name: ${client.getName()}</p>
    <c:choose>
        <c:when test="${exception != null}">
            <p>${exception}</p>
        </c:when>
        <c:when test="${balance != null}">
            <p>Your balance: ${balance}</p>
        </c:when>
    </c:choose>
</div>

<form id="balance-form" action="/atm" method="post">
    <input type="hidden" name="operationType" value="balance">
    <input class="form-submit" type="submit" value="Check your balance">
</form>

<form id="withdraw-form" action="/atm" method="post">
    <input type="hidden" name="operationType" value="withdraw">
    <p><span class="red withdraw"></span></p>
    <input type="text" name="withdraw" id="withdraw">
    <input class="form-submit" type="submit" value="Withdraw">
</form>

<form id="deposit-form" action="/atm" method="post">
    <input type="hidden" name="operationType" value="deposit">
    <p><span class="red deposit"></span></p>
    <input type="text" name="deposit" id="deposit">
    <input class="form-submit" type="submit" value="Deposit">
</form>

<jsp:include page="footer.jsp"/>