<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bank Application</title>

    <link href="resources/assets/css/reset.css" rel="stylesheet">
    <link href="resources/assets/css/style.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="resources/assets/js/validation.js"></script>
    <script type="text/javascript" src="resources/assets/js/form.js"></script>

</head>
<body>