<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="head.jsp"/>
<jsp:include page="header.jsp"/>

<div class="client-info">
    <p>
        <c:if test="${exception != null}">
            ${exception}
        </c:if>
    </p>
</div>

<form id="login" action="/login" method="post">
    <h2>Sign in</h2>
    <p><label for="clientName" id="label-name">Name:</label><span class="red name"></span></p>
    <p><input type="text" name="clientName" id="clientName"></p>
    <p><label for="password" id="label-password">Password:</label><span class="red password"></span></p>
    <p><input type="password" name="password" id="password"></p>
    <p><input class="form-submit" type="submit" value="Submit"></p>
</form>

<jsp:include page="footer.jsp"/>