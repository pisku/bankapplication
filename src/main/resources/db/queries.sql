-- select queries

select c.name, acc.balance, acc.type as "account type"
from client c
join account acc
on acc.client_id = c.id;

select c.name, acc.balance, acc.type as "account type"
from client c
join account acc
on acc.client_id = c.id
where acc.balance < 0
order by acc.balance asc;

select c.name, acc.balance, acc.type as "account type"
from client c
join account acc
on acc.client_id = c.id
where acc.balance > 1000
order by acc.balance desc;

-- update

update account as a1 set a1.balance = a1.balance +
(select a2.balance from account as a2 where a2.type = 's' and
a2.client_id = a1.client_id)
where a1.type = 'c';
update account set balance = 0 where type = 's';