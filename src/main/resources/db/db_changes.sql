-- create tables

CREATE TABLE IF NOT EXISTS bank (
	id int auto_increment not null,
	name varchar(255),
	primary key (id)
);

CREATE TABLE IF NOT EXISTS client (
    id int auto_increment not null,
    bank_id int,
    name varchar(255) unique,
    email varchar(255) unique,
    phone varchar(20),
    city varchar(100),
    gender varchar(1),
    initial_overdraft real,
    foreign key (bank_id) references bank (id),
    primary key (id)
);

CREATE TABLE IF NOT EXISTS account (
    id int auto_increment not null,
    balance real,
    overdraft real,
    type varchar(1),
    client_id int,
    primary key (id),
    foreign key (client_id) references client (id)
);

-- fill the database

insert into bank (id,name) values (1,'PS Bank');

insert into client (id, bank_id, name, email, phone, city, gender, initial_overdraft)
    values (1,1,'Piotr Skurski','peter@gmail.com','111-222-333','Cracov','m',1000);
insert into account (balance, overdraft, type, client_id) 
    values (1000,1000,'c',1);   
insert into account (balance, overdraft, type, client_id) 
    values (50000,0,'s',1);    
    
insert into client (id, bank_id, name, email, phone, city, gender, initial_overdraft)
    values (2,1,'John Kennedy','kennedy@wp.pl','555-555-555','New York','m',5000);
insert into account (balance, overdraft, type, client_id) 
    values (-555,5000,'c',2);   
insert into account (balance, overdraft, type, client_id) 
    values (12555,0,'s',2);   
    
insert into client (id, bank_id, name, email, phone, city, gender, initial_overdraft)
    values (3,1,'Veronica Smith','veronica@one.pl','999-111-222','Chicago','f',0);
insert into account (balance, overdraft, type, client_id) 
    values (999,0,'c',3);   
insert into account (balance, overdraft, type, client_id) 
    values (345,0,'s',3);  
    
insert into client (id, bank_id, name, email, phone, city, gender, initial_overdraft)
    values (4,1,'Jan Kowalski','kowalski@wp.pl','444-333-222','Katowice','m',550);
insert into account (balance, overdraft, type, client_id) 
    values (1999,550,'c',4);   
insert into account (balance, overdraft, type, client_id) 
    values (1345,0,'s',4); 
    
insert into client (id, bank_id, name, email, phone, city, gender, initial_overdraft)
    values (5,1,'Patrycja Bak','patrycja@gc.pl','894-423-432','Sydney','f',2000);
insert into account (balance, overdraft, type, client_id) 
    values (-1999,2000,'c',5);   
insert into account (balance, overdraft, type, client_id) 
    values (11345,0,'s',5);
